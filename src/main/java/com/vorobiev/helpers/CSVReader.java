package com.vorobiev.helpers;

import com.vorobiev.entity.Bar;
import com.vorobiev.entity.BarBuilder;
import com.vorobiev.entity.BarDate;
import com.vorobiev.ui.helpers.FXWindow;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.TreeSet;

public class CSVReader {
    private static int[] values;
    private static DateFormat format;
    private static BarBuilder builder;

    static {
        values = new int[4];
        format = new SimpleDateFormat("yyyy.MM.dd,HH:mm");
        builder = new BarBuilder();
    }

    public static TreeSet<Bar> getBarsFromCSV(String fileName) throws Exception {
        TreeSet<Bar> bars = new TreeSet();
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(FXWindow.path + fileName));
            while (scanner.hasNext()) {
                buildBar(scanner.next(), !scanner.hasNext(), bars);
            }
        } finally {
            if (scanner != null)
                scanner.close();
        }
        return bars;
    }

    public static Bar createBarFromCSVString(String string) {
        TreeSet<Bar> bars = new TreeSet();
        builder.clear();
        buildBar(string, true, bars);
        if (bars.isEmpty())
            return null;
        return bars.first();
    }

    private static void buildBar(String row, boolean isLast, TreeSet<Bar> bars) {
        String[] strings = row.split(",");
        String strDate = strings[0] + "," + strings[1];
        BarDate date;
        try {
            date = new BarDate(format.parse(strDate));
            if (date.dayOfWeek > 5 || date.dayOfWeek < 1) {
                System.out.println("Detected day of week more than 5 or less than 1: " + date.toString());
                return;
            }

        } catch (ParseException e) {
            System.out.println("Can not parse date from string of file.");
            return;
        }
        int k = 0;
        for (int i = 2; i < 6; i++) {
            float f = Float.valueOf(strings[i]);
            values[k++] = (int) (f * 10000);
        }
        int index = strings[6].indexOf(";");
        if (index != -1)
            strings[6] = strings[6].substring(0, index);
        builder.addData(values, date, Integer.valueOf(strings[6]), isLast, bars);
    }
}
