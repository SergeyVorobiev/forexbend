package com.vorobiev.helpers;

import com.vorobiev.entity.Bar;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.TreeSet;

public class IndicatorFactory {
    public static final LinkedHashMap<String, Boolean> indicatorNames = new LinkedHashMap();

    static {
        indicatorNames.put("day", false);
        indicatorNames.put("up", true);
        indicatorNames.put("down", true);
        indicatorNames.put("outer", true);
        indicatorNames.put("inner", true);
        indicatorNames.put("monday", true);
        indicatorNames.put("tuesday", true);
        indicatorNames.put("wednesday", true);
        indicatorNames.put("thursday", true);
        indicatorNames.put("friday", true);
        indicatorNames.put("range3days", false);
    }

    public static ArrayList<Bar> fillBaseIndicators(TreeSet<Bar> bars) {
        ArrayList<Bar> arrayBars = new ArrayList(bars);
        int curIndex = 0;
        for (int i = 0; i < arrayBars.size(); i++)
            fillBaseIndicators(arrayBars, i);
        return arrayBars;
    }

    public static void fillBaseIndicators(ArrayList<Bar> bars, int index) {
        Bar b = bars.get(index);
        b.setArithmeticIndicator("day", getDayOfWeek(b));
        b.setPredictableIndicator("up", b.isUp ? 1.0f : 0);
        b.setPredictableIndicator("down", b.isUp ? 0 : 1.0f);
        b.setPredictableIndicator("outer", getOuter(bars, index));
        b.setPredictableIndicator("inner", getInner(bars, index));
        b.setPredictableIndicator("monday", b.date.dayOfWeek == 1 ? 1.0f : 0);
        b.setPredictableIndicator("tuesday", b.date.dayOfWeek == 2 ? 1.0f : 0);
        b.setPredictableIndicator("wednesday", b.date.dayOfWeek == 3 ? 1.0f : 0);
        b.setPredictableIndicator("thursday", b.date.dayOfWeek == 4 ? 1.0f : 0);
        b.setPredictableIndicator("friday", b.date.dayOfWeek == 5 ? 1.0f : 0);
        b.setArithmeticIndicator("range3days", getRange3Days(bars, index));
    }

    private static float getDayOfWeek(Bar bar) {
        return bar.date.dayOfWeek;
    }

    private static Float getRange3Days(ArrayList<Bar> bars, int curIndexBar) {
        Bar curBar = bars.get(curIndexBar);
        Bar prevBar = getPrevBar(bars, curIndexBar, 2);
        if (prevBar == null)
            return null;
        return (float) (curBar.close - prevBar.open);
    }

    private static Float getOuter(ArrayList<Bar> bars, int curIndexBar) {
        Bar curBar = bars.get(curIndexBar);
        Bar prevBar = getPrevBar(bars, curIndexBar, 1);
        if (prevBar == null)
            return 0f;
        return curBar.max > prevBar.max && curBar.min < prevBar.min ? 1.0f : 0;
    }

    private static Float getInner(ArrayList<Bar> bars, int curIndexBar) {
        Bar curBar = bars.get(curIndexBar);
        Bar prevBar = getPrevBar(bars, curIndexBar, 1);
        if (prevBar == null)
            return 0f;
        return curBar.max < prevBar.max && curBar.min > prevBar.min ? 1.0f : 0;
    }

    public static Bar getPrevBar(ArrayList<Bar> bars, int curIndexBar, int prevIndexBar) {
        try {
            return bars.get(curIndexBar - prevIndexBar);
        } catch (Exception e) {
            return null;
        }
    }
}
