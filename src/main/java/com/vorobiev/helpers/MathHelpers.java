package com.vorobiev.helpers;

import com.vorobiev.entity.OrderResult;
import com.vorobiev.entity.Score;
import com.vorobiev.model.SettingsModel;

import java.util.ArrayList;

public class MathHelpers {
    public static ArrayList<Score> computeBetsWithOptimalF(SettingsModel model, ArrayList<Float> finalBalances) {
        float maxCapital = 0;
        finalBalances.clear();
        ArrayList<Score> result = new ArrayList();
        for (int i = 1; i <= 100; i++) {
            ArrayList<Score> scores = getOptimalF(model, i);
            float finalBalance;
            if (scores.isEmpty())
                finalBalance = 0;
            else
                finalBalance = scores.get(scores.size() - 1).balance;
            finalBalances.add(finalBalance);
            if (finalBalance > maxCapital) {
                maxCapital = finalBalance;
                result = scores;
            }
        }
        return result;
    }

    public static ArrayList<Score> computeBets(SettingsModel model, int optimalF) {
        return getOptimalF(model, optimalF);
    }

    private static ArrayList<Score> getOptimalF(SettingsModel model, int percent) {
        ArrayList<Score> scores = new ArrayList();
        ArrayList<OrderResult> results = new ArrayList(model.getSystemResults());
        if (!results.isEmpty() && results.get(results.size() - 1).getCloseDate() == null)
            results.remove(results.size() - 1);
        // Minimal volume of money to open order.
        final int minLot = 1000;
        float capital = model.getCapital();
        final float marginCall = model.getMarginCall();
        final float lever = model.getLever();

        // Minimal balance to avoid margin call.
        final float minBalance = capital * marginCall / 100;
        for (OrderResult result : model.getSystemResults()) {
            float bet = capital * percent / 100 * lever;
            bet = (int) (bet / minLot) * minLot;

            // Can not open order because of low money.
            if (bet < minLot) {
                scores.add(new Score(0, 0f, 0f, percent));
                return scores;
            }
            float realBet = bet / lever;

            // Can not open order because of margin call.
            if (capital - realBet <= minBalance) {
                scores.add(new Score(0, 0f, 0f, percent));
                return scores;
            }
            float subside = bet / 10000 * result.getMin();

            // We lost all money.
            if (capital - subside <= minBalance) {
                scores.add(new Score(0, 0f, 0f, percent));
                return scores;
            }
            float earnings = result.getTicks() * 0.0001f * bet;
            capital += earnings;
            scores.add(new Score((int) bet, earnings, capital, percent));
        }
        return scores;
    }


}
