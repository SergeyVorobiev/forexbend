package com.vorobiev.helpers;

import com.vorobiev.entity.Bar;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArithmeticOperationHelpers {

    private static class Operation {
        public final String operation;
        public final int bracket;

        public Operation(String operation, int bracket) {
            this.operation = operation;
            this.bracket = bracket;
        }
    }

    private static int bracket = 0;
    private static int curArithmeticIndex = 0;
    private static LinkedList<Float> numbers = new LinkedList();
    private static LinkedList<Operation> operations = new LinkedList();
    private static ArrayList<Bar> bars = null;
    private static int currentIndexOfSet = -1;

    public static void setVariableContext(ArrayList<Bar> bars) {
        ArithmeticOperationHelpers.bars = bars;
    }

    public static void setIndexOfCurrentSetOfVariables(int currentIndexOfSet) {
        ArithmeticOperationHelpers.currentIndexOfSet = currentIndexOfSet;
    }

    private static float executeLastOperation() {
        float b = numbers.pollLast();
        float a = numbers.pollLast();
        String operation = operations.pollLast().operation;
        if ("*-+/".contains(operation)) {
            return getArithmeticResult(a, operation, b);
        }
        return getLogicResult(a, operation, b);
    }

    public static boolean logicCompare(boolean a, String symbol, boolean b) {
        switch (symbol) {
            case "==":
                return a == b;
            case "!=":
                return a != b;
            default:
                throw new RuntimeException("Not recognized symbol for comparing booleans " + symbol + ".");
        }
    }

    public static float getArithmeticResult(float a, String operation, float b) {
        switch (operation) {
            case "*":
                return a * b;
            case "/":
                return a / b;
            case "+":
                return a + b;
            case "-":
                return a - b;
            default:
                throw new RuntimeException("Not recognized symbol for math operation " + operation + ".");
        }
    }

    public static boolean isNumber(String s) {
        try {
            Float.valueOf(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static float getLogicResult(float a, String symbol, float b) {
        switch (symbol) {
            case ">":
                return a > b ? 1 : 0;
            case ">=":
                return a >= b ? 1 : 0;
            case "==":
                return a == b ? 1 : 0;
            case "<=":
                return a <= b ? 1 : 0;
            case "<":
                return a < b ? 1 : 0;
            case "!=":
                return a != b ? 1 : 0;
            case "&&":
                boolean ba = (int) a == 1 ? true : false;
                boolean bb = (int) b == 1 ? true : false;
                return ba && bb == true ? 1 : 0;
            case "||":
                boolean ba2 = (int) a == 1 ? true : false;
                boolean bb2 = (int) b == 1 ? true : false;
                return ba2 || bb2 == true ? 1 : 0;
            default:
                throw new RuntimeException("Not recognized symbol for comparing numbers" + symbol + ".");
        }
    }

    private static int getPriority(String symbol) {
        switch (symbol) {
            case "*":
            case "/":
                return 3;
            case "+":
            case "-":
                return 2;
            case "(":
                return 1;
            case ")":
                return -1;
            case "||":
                return 4;
            case "&&":
                return 5;
            case "==":
            case "!=":
            case ">":
            case "<":
            case ">=":
            case "<=":
                return 6;
            default:
                throw new RuntimeException("Not recognized symbol for getting priority " + symbol + ".");
        }
    }

    public static float computeArithmeticExpression(String expression) {
        if (expression.substring(0, 1).equals("-"))
            expression = "0 " + expression;
        return computeArithmeticExpression(parseArithmeticString(expression));
    }

    private static float computeArithmeticExpression(LinkedList<Object> objects) {
        for (Object o : objects) {
            if (o instanceof Float) {
                numbers.add((Float) o);
            } else {
                int result = getPriority((String) o);
                if (result == 1 || result == -1) {
                    bracket += result;
                    continue;
                }
                if (operations.isEmpty()) {
                    operations.add(new Operation((String) o, bracket));
                    continue;
                }
                Operation curOper = new Operation((String) o, bracket);
                int curPriority = getPriority(curOper.operation);
                while (true) {
                    if (operations.isEmpty())
                        break;
                    Operation prevOper = operations.getLast();
                    int prevPriority = getPriority(prevOper.operation);
                    if (prevOper.bracket > curOper.bracket)
                        numbers.add(executeLastOperation());
                    else if (prevOper.bracket == curOper.bracket && prevPriority >= curPriority) {
                        numbers.add(executeLastOperation());
                    } else {
                        break;
                    }
                }
                operations.add(curOper);
            }
        }
        while (!operations.isEmpty())
            numbers.add(executeLastOperation());
        if (bracket != 0 || numbers.size() != 1)
            throw new RuntimeException("Wrong arithmetic expression.");
        return numbers.pollLast();
    }

    private static LinkedList<Object> parseArithmeticString(String s) {
        s = s.trim();
        LinkedList<Object> objects = new LinkedList();
        curArithmeticIndex = 0;
        while (true) {
            Object o = getArithmeticSymbol(s);
            curArithmeticIndex++;
            if (o == null)
                break;
            objects.add(o);
        }
        return objects;
    }

    private static Object getArithmeticSymbol(String s) {
        boolean isNumber = false;
        boolean isVariable = false;
        String number = "";
        String variable = "";
        for (; curArithmeticIndex < s.length(); curArithmeticIndex++) {
            String symbol = s.substring(curArithmeticIndex, curArithmeticIndex + 1);
            if (!isNumber && !isVariable && isNumber(symbol))
                isNumber = true;

            if (!isNumber && !isVariable) {
                if (symbol.equals(" "))
                    continue;
                String pSymbol = getSymbol(s, symbol);
                if (pSymbol != null) {
                    return pSymbol;
                } else if (symbol.equals("-")) {
                    if (curArithmeticIndex + 1 == s.length())
                        throw new RuntimeException("Wrong arithmetic expression.");
                    String nextSymbol = s.substring(curArithmeticIndex + 1, curArithmeticIndex + 2);
                    if (isNumber(nextSymbol)) {
                        isNumber = true;
                        number = symbol;
                    } else if (nextSymbol.equals("(") || nextSymbol.equals(" ")) {
                        return symbol;
                    } else {
                        variable = symbol;
                        isVariable = true;
                    }
                } else {
                    isVariable = true;
                    variable = symbol;
                }
            } else {
                if (symbol.equals(")") || symbol.equals(" ")) {
                    --curArithmeticIndex;
                    return isNumber ? Float.valueOf(number) : getValueFromVariable(variable);
                }
                if (isNumber)
                    number += symbol;
                else
                    variable += symbol;
            }
        }
        return isNumber ? Float.valueOf(number) : isVariable ? variable : null;
    }

    private static String getSymbol(String s, String symbol) {
        if (symbol.equals("(") || symbol.equals(")") || symbol.equals("+") || symbol.equals("*") || symbol.equals("/")) {
            return symbol;
        } else if (symbol.equals(">") || symbol.equals("<")) {
            String nextSymbol = s.substring(curArithmeticIndex + 1, curArithmeticIndex + 2);
            if (nextSymbol.equals("=")) {
                curArithmeticIndex++;
                return symbol + nextSymbol;
            }
            return symbol;
        } else if (symbol.equals("=") || symbol.equals("!")) {
            String nextSymbol = s.substring(curArithmeticIndex + 1, curArithmeticIndex + 2);
            if (nextSymbol.equals("=")) {
                curArithmeticIndex++;
                return symbol + nextSymbol;
            } else {
                throw new RuntimeException("Syntax error in arithmetic expression.");
            }
        } else if (symbol.equals("&") || symbol.equals("|")) {
            String nextSymbol = s.substring(curArithmeticIndex + 1, curArithmeticIndex + 2);
            if ((symbol.equals("&") && nextSymbol.equals("&")) || (symbol.equals("|") && nextSymbol.equals("|"))) {
                curArithmeticIndex++;
                return symbol + nextSymbol;
            } else {
                throw new RuntimeException("Syntax error in arithmetic expression.");
            }
        }
        return null;
    }

    private static float getValueFromVariable(String variable) {
        if (bars == null)
            throw new RuntimeException("Variable context to parse expression with variables is not exists.");
        if (currentIndexOfSet == -1)
            throw new RuntimeException("Current index of set of variables does not set.");
        String[] parts = variable.split("\\$");
        int prevIndex;
        if (parts.length == 1) {
            prevIndex = 0;
        } else if (parts.length == 2) {
            prevIndex = Integer.valueOf(parts[1]);
        } else {
            throw new RuntimeException("Syntax error for variable " + variable + ".");
        }
        Bar bar = IndicatorFactory.getPrevBar(bars, currentIndexOfSet, prevIndex);
        if (bar == null)
            throw new RuntimeException("Bar with index " + (currentIndexOfSet - prevIndex) + " does not exist.");
        String first = parts[0].substring(0, 1);
        Float result;
        if (first.equals("#"))
            result = bar.getPredictableIndicator(parts[0].substring(1));
        else
            result = bar.getArithmeticIndicator(parts[0]);
        if (result == null)
            throw new RuntimeException("Indicator with name " + parts[0] + " does not exist.");
        return result;
    }

    public static Float computeLogicExpression(String expression) {
        return computeArithmeticExpression(parseArithmeticString(expression));
    }
}
