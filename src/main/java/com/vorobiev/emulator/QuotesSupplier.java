package com.vorobiev.emulator;

import com.vorobiev.ui.helpers.FXWindow;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class QuotesSupplier implements Runnable {
    private ArrayList<String> quotes = new ArrayList();
    private int index = 0;

    public QuotesSupplier() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(FXWindow.path + "EURUSD2012.csv"));
            while (scanner.hasNext()) {
                quotes.add(scanner.next());
            }
        } catch (Exception e) {
            System.out.println("Cannot read quotes from file in QuotesSupplier.");
        } finally {
            if (scanner != null)
                scanner.close();
        }
    }

    @Override
    public void run() {
        while (true) {
            sleep(2000);
            setQuotesToFile("D:\\quotes.txt");
        }
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            System.out.println("Can not sleep.");
        }
    }

    private void setQuotesToFile(String filePath) {
        if (index >= quotes.size())
            return;
        try {
            PrintWriter p = new PrintWriter(new FileOutputStream(filePath));
            String s = quotes.get(index++);
            p.write(s);
            System.out.println("Write to file: " + s);
            p.close();
        } catch (Exception e) {
            System.out.println("Can not set quotes to file.");
        }
    }
}
