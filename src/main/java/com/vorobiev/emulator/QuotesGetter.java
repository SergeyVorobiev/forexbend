package com.vorobiev.emulator;

import com.vorobiev.model.MainModel;
import com.vorobiev.model.QuotationsModel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class QuotesGetter implements Runnable {
    private MainModel model;

    // TODO: reattach model after loading.
    public QuotesGetter(MainModel model) {
        this.model = model;
        try {
            new PrintWriter(new FileOutputStream("D:\\quotes.txt")).close();
        } catch (Exception e) {
            System.out.println("Can not clean file on running.");
        }
    }

    @Override
    public void run() {
        while (true) {
            sleep(1000);
            String s = getQuotesFromFile("D:\\quotes.txt");
            // TODO: very dangerous need to synchronize getting quotations model.
            if (s != null) {
                if (model.quotationsModels.size() > 1) {
                    QuotationsModel m = (QuotationsModel) model.quotationsModels.get(1);
                    m.addQuotationOnUI(s);
                }
            }
        }
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            System.out.println("Can not sleep.");
        }
    }

    private String getQuotesFromFile(String filePath) {
        String str = null;
        try {
            Scanner s = new Scanner(new FileInputStream(filePath));
            if (s.hasNext())
                str = s.next();
            s.close();
            System.out.println("Read from file: " + str);
            new PrintWriter(new FileOutputStream(filePath)).close();
            System.out.println("Clean file.");
        } catch (Exception e) {
            System.out.println("Can not get quotes from file.");
        }
        return str;
    }
}
