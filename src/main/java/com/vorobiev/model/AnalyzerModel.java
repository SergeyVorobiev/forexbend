package com.vorobiev.model;

import com.vorobiev.entity.*;
import com.vorobiev.helpers.Container;

import java.util.ArrayList;
import java.util.TreeSet;

public class AnalyzerModel extends Model {
    public final MainModel model;
    private TreeSet<Float> set = new TreeSet();
    public final Container<String> daysBeforeReference = new Container();
    public final ArrayList<String> daysBefore = new ArrayList();
    public final Container<String> indicatorAnalyzerReference = new Container();
    public final ArrayList<String> indicatorAnalyzer = new ArrayList();
    public final Container<ChildModel> systemAnalyzerReference = new Container();

    public AnalyzerModel(MainModel model) {
        this.model = model;
        daysBeforeReference.value = "1";
        daysBefore.add("1");
        daysBefore.add("2");
        daysBefore.add("3");
        systemAnalyzerReference.value = model.settingsModels.isEmpty() ? null : model.settingsModels.get(0);
        if (systemAnalyzerReference.value != null) {
            SystemInfo info = ((SettingsModel) systemAnalyzerReference.value).getSystemInfo();
            if (info != null) {
                for (UserIndicatorInfo indicator : model.getQuotationsByUniqNumber(info.uniqNumberOfQuotations).userIndicators) {
                    indicatorAnalyzer.add(indicator.name);
                }
            }
        }
        indicatorAnalyzerReference.value = indicatorAnalyzer.isEmpty() ? "" : indicatorAnalyzer.get(0);
        indicatorAnalyzer.add("");
    }

    public void analyze() {
        test();
    }

    public void test() {
        set.clear();
        ArrayList<OrderResult> results = ((SettingsModel) model.settingsModels.get(0)).systemResults;
        ArrayList<OrderResult> profits = new ArrayList();
        ArrayList<OrderResult> loses = new ArrayList();
        for (OrderResult r : results) {
            if (r.getTicks() > 0)
                profits.add(r);
            else
                loses.add(r);
        }

        long number = ((SettingsModel) model.settingsModels.get(0)).getSystemInfo().uniqNumberOfQuotations;
        ArrayList<Bar> allQuot = model.getQuotationsByUniqNumber(number).getIndicatorBars();
        ArrayList<Bar> beforeOpenProfit = getBarsBeforeOpenOrClose(allQuot, profits, 1, true);
        ArrayList<Bar> beforeOpenLose = getBarsBeforeOpenOrClose(allQuot, loses, 1, true);
        Distribution d = getDistribution(beforeOpenProfit, "range3days");
        for (int i = 0; i < d.values.length; i++)
            System.out.println(d.names[i] + " " + d.values[i]);
        d = getDistribution(beforeOpenLose, "range3days");
        System.out.println("-------------------------");
        for (int i = 0; i < d.values.length; i++)
            System.out.println(d.names[i] + " " + d.values[i]);
    }

    private class Distribution {
        public final String[] names = new String[11];
        public int[] values;
    }

    public Distribution getDistribution(ArrayList<Bar> bars, String indicator) {
        ArrayList<Float> result = getListValuesOfIndicator(bars, indicator);
        int[] distrib = new int[11];
        Distribution d = new Distribution();
        if (!result.isEmpty()) {
            result.sort(Float::compare);
            float min = result.get(0);
            float max = result.get(result.size() - 1);
            float sum = max - min;
            float from = min;
            float to;
            float k = sum / 10;
            for (int i = 0; i < 10; i++) {
                to = from + k;
                d.names[i] = "[" + from + "; " + to + ")";
                from = to;
            }
            d.names[10] = String.valueOf(max);

            for (Float r : result) {
                int index = (int) ((r - min) / k);
                distrib[index]++;
            }
        }
        d.values = distrib;
        return d;
    }

    public ArrayList<Float> getListValuesOfIndicator(ArrayList<Bar> bars, String indicator) {
        ArrayList<Float> result = new ArrayList();
        for (Bar b : bars) {
            Float f = b.getArithmeticIndicator(indicator);
            if (f != null)
                result.add(f);
        }
        return result;
    }

    public ArrayList<Bar> getBarsBeforeOpenOrClose(ArrayList<Bar> allBars, ArrayList<OrderResult> targetResults, int dayBefore, boolean beforeOpen) {
        ArrayList<Bar> values = new ArrayList();
        int k = 0;
        for (OrderResult r : targetResults) {
            while (k < allBars.size()) {
                Bar b = allBars.get(k);
                BarDate d = beforeOpen ? r.getOpenDate() : r.getCloseDate();
                if (d.equals(b.date)) {
                    try {
                        values.add(allBars.get(k - dayBefore));
                        break;
                    } catch (Exception e) {
                        ;
                    }
                }
                k++;
            }
        }
        return values;
    }
}
