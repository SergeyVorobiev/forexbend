package com.vorobiev.model;

import com.vorobiev.entity.Bar;
import com.vorobiev.entity.UserIndicatorInfo;
import com.vorobiev.helpers.ArithmeticOperationHelpers;
import com.vorobiev.helpers.CSVReader;
import com.vorobiev.helpers.IndicatorFactory;
import com.vorobiev.ui.view.MLSubscriber;
import javafx.application.Platform;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeSet;

public class QuotationsModel extends ChildModel implements Externalizable {
    private ArrayList<Bar> indicatorBars;
    private long uniqNumber = -1;
    public final ArrayList<UserIndicatorInfo> userIndicators = new ArrayList();

    public QuotationsModel() {
        super(null, "none", 0);
        indicatorBars = new ArrayList();
    }

    public QuotationsModel(String title, int id, TreeSet<Bar> defaultBars, MainModel mainModel) {
        super(mainModel, title, id);
        this.indicatorBars = IndicatorFactory.fillBaseIndicators(defaultBars);
        for (Map.Entry<String, Boolean> entry : IndicatorFactory.indicatorNames.entrySet())
            userIndicators.add(new UserIndicatorInfo(entry.getKey(), "", entry.getValue()));
        uniqNumber = mainModel.getUniqQuotationsNumber();
    }

    public ArrayList<Bar> getIndicatorBars() {
        return indicatorBars;
    }

    public void addIndicator(String name, String formula, boolean isPredictable) {
        ArithmeticOperationHelpers.setVariableContext(indicatorBars);
        for (int i = 0; i < indicatorBars.size(); i++)
            computeIndicatorForBar(name, formula, isPredictable, indicatorBars.get(i), i);
        userIndicators.add(new UserIndicatorInfo(name, formula, isPredictable));
        send(MLSubscriber.UPDATE_QUOTATIONS_TABLE);
    }

    public long getUniqNumber() {
        return uniqNumber;
    }

    public void addQuotation(String csvQuotation) {
        Bar b = CSVReader.createBarFromCSVString(csvQuotation);
        addBar(b);
        send(MLSubscriber.UPDATE_QUOTATIONS_TABLE);
        for (ChildModel m : mainModel.settingsModels) {
            SettingsModel sm = (SettingsModel) m;
            if (sm.quotationsReference.value.toString().equals(this.toString())) {
                // TODO: implements chek box for auto update logic.
                if (true) {
                    sm.runOnce();
                }
            }
        }
    }

    public void addQuotationOnUI(String csvQuotation) {
        Platform.runLater(() -> addQuotation(csvQuotation));
    }

    private void addBar(Bar bar) {
        ArithmeticOperationHelpers.setVariableContext(indicatorBars);
        Bar lastBar = indicatorBars.get(indicatorBars.size() - 1);
        if (lastBar.date.compareTo(bar.date) >= 0) {
            System.out.println("Wrong bar date: " + lastBar.date.toString() + " : " + bar.date.toString());
            return;
        }
        indicatorBars.add(bar);
        int index = indicatorBars.size() - 1;
        IndicatorFactory.fillBaseIndicators(indicatorBars, index);
        lastBar = indicatorBars.get(index);
        for (UserIndicatorInfo info : userIndicators)
            computeIndicatorForBar(info.name, info.formula, info.isPredictable, lastBar, index);
    }

    private void computeIndicatorForBar(String name, String formula, boolean isPredictable, Bar bar, int index) {
        ArithmeticOperationHelpers.setIndexOfCurrentSetOfVariables(index);
        try {
            if (isPredictable)
                bar.setPredictableIndicator(name, ArithmeticOperationHelpers.computeLogicExpression(formula));
            else
                bar.setArithmeticIndicator(name, ArithmeticOperationHelpers.computeArithmeticExpression(formula));
        } catch (Exception e) {
            System.out.println("Wrong formula: " + formula + ".");
            if (isPredictable)
                bar.setPredictableIndicator(name, 0f);
            else
                bar.setArithmeticIndicator(name, null);
        }
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeLong(uniqNumber);
        out.writeInt(indicatorBars.size());
        for (Bar b : indicatorBars)
            out.writeObject(b);
        out.writeInt(id);
        out.writeObject(title);
        out.writeInt(userIndicators.size());
        for (UserIndicatorInfo info : userIndicators) {
            out.writeObject(info);
        }
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        uniqNumber = in.readLong();
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            indicatorBars.add((Bar) in.readObject());
        }
        id = in.readInt();
        title = (String) in.readObject();
        size = in.readInt();
        for (int i = 0; i < size; i++) {
            userIndicators.add((UserIndicatorInfo) in.readObject());
        }
    }
}
