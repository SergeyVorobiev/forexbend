package com.vorobiev.model;

public interface Notifier {
    void send(int signal);
}
