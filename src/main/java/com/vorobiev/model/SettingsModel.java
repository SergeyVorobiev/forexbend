package com.vorobiev.model;

import com.vorobiev.entity.*;
import com.vorobiev.helpers.Container;
import com.vorobiev.helpers.MathHelpers;
import com.vorobiev.ui.view.MLSubscriber;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.TreeMap;

public class SettingsModel extends ChildModel implements Externalizable {
    private String capital;
    public final Container<String> leverReference = new Container();
    public final Container<ChildModel> quotationsReference = new Container();
    public final ArrayList<String> levers = new ArrayList();
    public final Container<String> callReference = new Container();
    public final ArrayList<String> calls = new ArrayList();
    public final Container<String> typeReference = new Container();
    public final ArrayList<String> types = new ArrayList();
    public final Container<String> orderDaysReference = new Container();
    public final ArrayList<String> ordersDays = new ArrayList();
    public final Container<String> profitDaysReference = new Container();
    public final ArrayList<String> profitsDays = new ArrayList();
    public final Container<String> openIndicatorReference = new Container();
    public final Container<String> closeIndicatorReference = new Container();
    public final ArrayList<String> openIndicators = new ArrayList();
    public final ArrayList<String> closeIndicators = new ArrayList();
    private long uniqNumber;
    private SystemInfo systemInfo = null;
    private String stopSize;
    public final ArrayList<OrderResult> systemResults = new ArrayList();
    public final ArrayList<Score> scores = new ArrayList();
    public final ArrayList<Float> finalBalances = new ArrayList();

    public SettingsModel() {
        this("", 0, null);
    }

    public SettingsModel(String title, int id, MainModel model) {
        super(model, title, id);
        capital = "1000";
        levers.add("1");
        levers.add("10");
        levers.add("100");
        calls.add("0");
        calls.add("10");
        calls.add("20");
        calls.add("30");
        calls.add("40");
        types.add("long");
        types.add("short");
        ordersDays.add("1");
        ordersDays.add("2");
        ordersDays.add("3");
        ordersDays.add("4");
        profitsDays.add("1");
        profitsDays.add("2");
        profitsDays.add("3");
        profitsDays.add("4");
        leverReference.value = "100";
        callReference.value = "0";
        typeReference.value = "long";
        orderDaysReference.value = "1";
        profitDaysReference.value = "1";
        stopSize = "0";
        openIndicators.add("none");
        closeIndicators.add("none");
        openIndicatorReference.value = openIndicators.get(0);
        closeIndicatorReference.value = closeIndicators.get(0);
        if (model != null)
            quotationsReference.value = model.quotationsModels.get(0);
        uniqNumber = mainModel.getUniqSystemSettingsNumber();
    }

    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    public void runSystem() {
        SimpleCloser closer = new SimpleCloser(this);
        ArrayList<Bar> bars = getBars();
        if (bars == null)
            return;
        long newNumber = ((QuotationsModel) quotationsReference.value).getUniqNumber();
        long oldNumber = systemInfo == null ? newNumber : systemInfo.uniqNumberOfQuotations;
        SystemInfo oldInfo = systemInfo;
        systemInfo = new SystemInfo(closer, getCapital(), getOpenIndicator(), newNumber);
        Engine.runAll(bars, systemResults, systemInfo);
        scores.clear();
        scores.addAll(MathHelpers.computeBetsWithOptimalF(this, finalBalances));
        systemInfo.compute(systemResults, scores);
        send(MLSubscriber.UPDATE_SYSTEM_RESULT);
        if (newNumber != oldNumber)
            mainModel.findMonitorModelByQuotationsNumber(oldNumber).removeOutdatedSystems(oldInfo);
        mainModel.findMonitorModelByQuotationsNumber(newNumber).addOrUpdateSystem(this);
    }

    public long getUniqNumber() {
        return uniqNumber;
    }

    public void runOnce() {
        if (systemInfo == null)
            return;
        ArrayList<Bar> bars = getBars();
        if (bars == null)
            return;
        if (bars.size() < 2)
            return;
        Engine.openUpdateClose(bars.get(bars.size() - 1), systemResults, systemInfo.closer, bars.get(bars.size() - 2).getPredictableIndicator(systemInfo.getOpenIndicator()).intValue());
        scores.clear();
        scores.addAll(MathHelpers.computeBets(this, systemInfo.getOptimalF()));
        systemInfo.compute(systemResults, scores);
        send(MLSubscriber.UPDATE_SYSTEM_RESULT_WITHOUT_F);
        mainModel.findMonitorModelByQuotationsNumber(systemInfo.uniqNumberOfQuotations).addOrUpdateSystem(this);
    }

    private ArrayList<Bar> getBars() {
        if (openIndicatorReference.value.equals("none"))
            return null;
        ArrayList<Bar> bars = null;
        for (ChildModel m : getMainModel().quotationsModels) {
            if (m.toString().equals(quotationsReference.value.toString())) {
                if (m.id != 0)
                    bars = ((QuotationsModel) m).getIndicatorBars();
                break;
            }
        }
        return bars;
    }

    public void updateIndicators() {
        openIndicators.clear();
        closeIndicators.clear();
        openIndicators.add("none");
        closeIndicators.add("none");
        QuotationsModel m = (QuotationsModel) quotationsReference.value;
        if (m == null || m.getIndicatorBars().isEmpty()) {
            openIndicatorReference.value = "none";
            closeIndicatorReference.value = "none";
        } else {
            for (String indicatorName : m.getIndicatorBars().get(0).getPredictableIndicatorNames()) {
                openIndicators.add(indicatorName);
                closeIndicators.add(indicatorName);
            }
            if (!openIndicators.contains(openIndicatorReference.value))
                openIndicatorReference.value = openIndicators.get(0);
            if (!closeIndicators.contains(closeIndicatorReference.value))
                closeIndicatorReference.value = closeIndicators.get(0);
        }
        send(MLSubscriber.UPDATE_INDICATORS_FOR_SYSTEMS);
    }

    public int getBalance(int percent) {
        float balance = 0;
        try {
            balance = finalBalances.get(percent - 1);
        } catch (Exception e) {
            ;
        }
        return (int) balance;
    }

    public void updateQuotationsList() {
        if (!getMainModel().quotationsModels.contains(quotationsReference.value))
            quotationsReference.value = getMainModel().quotationsModels.get(0);
        send(MLSubscriber.UPDATE_SYSTEMS_QUOTATIONS_LIST);
    }

    public void setStopSize(String stopSize) {
        this.stopSize = stopSize;
    }

    public float getCapital() {
        return Float.valueOf(capital);
    }

    public int getLever() {
        return Integer.valueOf(leverReference.value);
    }

    public int getMarginCall() {
        return Integer.valueOf(callReference.value);
    }

    public boolean getType() {
        return typeReference.value.equals("long") ? true : false;
    }

    public int getOrderDays() {
        return Integer.valueOf(orderDaysReference.value);
    }

    public int getProfitDays() {
        return Integer.valueOf(profitDaysReference.value);
    }

    public String getOpenIndicator() {
        return openIndicatorReference.value;
    }

    public String getCloseIndicator() {
        return closeIndicatorReference.value;
    }

    public int getStopSize() {
        if (stopSize == null || stopSize.equals(""))
            return 0;
        return -Integer.valueOf(stopSize);
    }

    public String getStopSizeString() {
        return stopSize;
    }

    public BarDate getStartDate() {
        return null;
    }

    public BarDate getEndDate() {
        return null;
    }

    public ArrayList<OrderResult> getSystemResults() {
        return this.systemResults;
    }

    public float getFinalBalance() {
        if (scores.isEmpty())
            return 0;
        else
            return scores.get(scores.size() - 1).balance;
    }

    public TreeMap<String, Integer> getPercentProfitOfDays() {
        TreeMap<String, Integer> map = new TreeMap();
        String[] daysOfWeek = {"Пн", "Вт", "Ср", "Чт", "Пт"};
        if (systemInfo != null)
            for (int i = 0; i < systemInfo.getProfitPercentByDaysOfWeek().length; i++)
                map.put(daysOfWeek[i], systemInfo.getProfitPercentByDaysOfWeek()[i]);
        return map;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeLong(uniqNumber);
        out.writeInt(id);
        out.writeObject(title);
        out.writeObject(capital);
        out.writeObject(leverReference.value);
        out.writeObject(callReference.value);
        out.writeObject(typeReference.value);
        out.writeObject(orderDaysReference.value);
        out.writeObject(profitDaysReference.value);
        out.writeObject(openIndicatorReference.value);
        out.writeObject(closeIndicatorReference.value);
        out.writeInt(openIndicators.size());
        for (String s : openIndicators)
            out.writeObject(s);
        out.writeInt(closeIndicators.size());
        for (String s : closeIndicators)
            out.writeObject(s);
        out.writeObject(systemInfo);
        out.writeObject(stopSize);

        out.writeInt(systemResults.size());
        for (OrderResult r : systemResults)
            out.writeObject(r);
        out.writeInt(scores.size());
        for (Score s : scores)
            out.writeObject(s);
        out.writeInt(finalBalances.size());
        for (float f : finalBalances)
            out.writeFloat(f);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        uniqNumber = in.readLong();
        id = in.readInt();
        title = (String) in.readObject();
        capital = (String) in.readObject();
        leverReference.value = (String) in.readObject();
        callReference.value = (String) in.readObject();
        typeReference.value = (String) in.readObject();
        orderDaysReference.value = (String) in.readObject();
        profitDaysReference.value = (String) in.readObject();
        openIndicatorReference.value = (String) in.readObject();
        closeIndicatorReference.value = (String) in.readObject();
        int size = in.readInt();
        for (int i = 0; i < size; i++)
            openIndicators.add((String) in.readObject());
        size = in.readInt();
        for (int i = 0; i < size; i++)
            closeIndicators.add((String) in.readObject());
        systemInfo = (SystemInfo) in.readObject();
        stopSize = (String) in.readObject();
        size = in.readInt();
        for (int i = 0; i < size; i++)
            systemResults.add((OrderResult) in.readObject());
        size = in.readInt();
        for (int i = 0; i < size; i++)
            scores.add((Score) in.readObject());
        size = in.readInt();
        for (int i = 0; i < size; i++)
            finalBalances.add(in.readFloat());
    }
}
