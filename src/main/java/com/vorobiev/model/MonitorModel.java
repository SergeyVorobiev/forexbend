package com.vorobiev.model;

import com.vorobiev.entity.SystemInfo;
import com.vorobiev.ui.view.MLSubscriber;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;

public class MonitorModel extends ChildModel implements Externalizable {
    public final ArrayList<SystemListModel> systemListModels = new ArrayList();
    private long uniqNumberOfQuotations;

    public MonitorModel(QuotationsModel model) {
        super(model.mainModel, model.title, model.id);
        uniqNumberOfQuotations = model.getUniqNumber();
        for (ChildModel m : mainModel.settingsModels) {
            SettingsModel sm = (SettingsModel) m;
            SystemInfo info = sm.getSystemInfo();
            if (info != null) {
                if (uniqNumberOfQuotations == info.uniqNumberOfQuotations) {
                    systemListModels.add(new SystemListModel(sm.getUniqNumber(), info));
                }
            }
        }
    }

    public void removeOutdatedSystems(SystemInfo oldInfo) {
        for (SystemListModel m : systemListModels) {
            if (m.info == oldInfo) {
                systemListModels.remove(m);
                send(MLSubscriber.REMOVE_OUTDATED_SYSTEM_MONITOR);
                break;
            }
        }
    }

    public void addOrUpdateSystem(SettingsModel sm) {
        long uniqNumberOfSystem = sm.getUniqNumber();
        for (SystemListModel m : systemListModels) {
            if (m.getUniqNumberOfSystem() == uniqNumberOfSystem) {
                m.updateGraphic();
                return;
            }
        }
        systemListModels.add(new SystemListModel(uniqNumberOfSystem, sm.getSystemInfo()));
        send(MLSubscriber.ADD_SYSTEM_MONITOR);
    }

    public long getUniqNumberOfQuotations() {
        return uniqNumberOfQuotations;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }
}
