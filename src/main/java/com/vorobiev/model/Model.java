package com.vorobiev.model;

import com.vorobiev.ui.view.Subscriber;

import java.util.ArrayList;

public class Model implements Notifier {

    // public Set<Subscriber> subscribers = Collections.newSetFromMap(new WeakHashMap<Subscriber, Boolean>());
    public ArrayList<Subscriber> subscribers = new ArrayList();

    public void send(int signal) {
        for (Subscriber sub : subscribers)
            sub.update(signal);
    }
}
