package com.vorobiev.model;

import com.vorobiev.ui.view.MLSubscriber;

public class ChildModel extends Model {
    protected MainModel mainModel;
    protected int id;
    protected String title;

    public ChildModel(MainModel model, String title, int id) {
        this.mainModel = model;
        this.title = title;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        send(MLSubscriber.UPDATE_TITLE);
    }

    public MainModel getMainModel() {
        return mainModel;
    }

    public void setMainModel(MainModel model) {
        this.mainModel = model;
    }

    @Override
    public String toString() {
        return id + ". " + title;
    }
}
