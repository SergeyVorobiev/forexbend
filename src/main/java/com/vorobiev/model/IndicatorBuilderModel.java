package com.vorobiev.model;

import com.vorobiev.helpers.Container;
import com.vorobiev.ui.view.MLSubscriber;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class IndicatorBuilderModel extends ChildModel implements Externalizable {
    private String indicatorFormula;
    private String indicatorName;
    public final Container<ChildModel> quotationsReference = new Container();

    public IndicatorBuilderModel() {
        super(null, "", 0);
    }

    public IndicatorBuilderModel(String title, int id, MainModel mainModel) {
        super(mainModel, title, id);
        indicatorFormula = "";
        indicatorName = "";
        quotationsReference.value = mainModel.quotationsModels.get(0);
    }

    public void updateQuotationsList() {
        if (!getMainModel().quotationsModels.contains(quotationsReference.value))
            quotationsReference.value = getMainModel().quotationsModels.get(0);
        send(MLSubscriber.UPDATE_INDICATORS_QUOTATIONS_LIST);
    }

    public void computePredictableIndicator(String name, String desc, int selectedIndexQuotationsSet) {
        indicatorName = name;
        indicatorFormula = desc;
        QuotationsModel qModel = (QuotationsModel) getMainModel().quotationsModels.get(selectedIndexQuotationsSet);
        qModel.addIndicator(name, desc, true);
        for (ChildModel m : getMainModel().settingsModels) {
            ((SettingsModel) m).updateIndicators();
        }
    }

    public String getIndicatorFormula() {
        return indicatorFormula;
    }

    public String getIndicatorName() {
        return indicatorName;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(indicatorFormula);
        out.writeObject(indicatorName);
        out.writeObject(title);
        out.writeInt(id);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        indicatorFormula = (String) in.readObject();
        indicatorName = (String) in.readObject();
        title = (String) in.readObject();
        id = in.readInt();
    }
}
