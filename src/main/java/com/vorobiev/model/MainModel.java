package com.vorobiev.model;

import com.vorobiev.helpers.CSVReader;
import com.vorobiev.ui.view.BarsView;
import com.vorobiev.ui.view.MLSubscriber;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;

public class MainModel extends Model implements Externalizable {
    //private TreeSet<Bar> defaultBars;
    public final ArrayList<ChildModel> quotationsModels = new ArrayList();
    public final ArrayList<IndicatorBuilderModel> builderModels = new ArrayList();
    public final ArrayList<ChildModel> settingsModels = new ArrayList();
    public final ArrayList<MonitorModel> monitorModels = new ArrayList();
    private BarsView barsView;
    private BarsView predictedBarsView;
    public AnalyzerModel analyzerModel;
    private long uniqQuotationsCounter = 0;
    private long uniqSystemSettingsCounter = 0;

    public MainModel() throws Exception {
        //defaultBars = CSVReader.getBarsFromCSV("EURUSD240.csv");
        //quotationsModels.add(new QuotationsModel("EURUSD240", quotationsModels.size(), defaultBars));
        //quotationsModels.add(new QuotationsModel("EURUSD1440", quotationsModels.size(), CSVReader.getBarsFromCSV("EURUSD1440.csv")));
    }

    /*
    public TreeSet<Bar> getDefaultBars() {
        return defaultBars;
    }
    */

    public void setPredictedBarsView(BarsView predictedBarsView) {
        this.predictedBarsView = predictedBarsView;
    }

    public BarsView getPredictedBarsView() {
        return predictedBarsView;
    }

    public void setBarsView(BarsView barsView) {
        this.barsView = barsView;
    }

    public BarsView getBarsView() {
        return barsView;
    }

    public void addQuotations(String path) {
        try {
            QuotationsModel qm = new QuotationsModel(path, quotationsModels.size(), CSVReader.getBarsFromCSV(path), this);
            quotationsModels.add(qm);
            monitorModels.add(new MonitorModel(qm));
            send(MLSubscriber.ADD_QUOTATIONS);
            updateSettingsModelsData();
        } catch (Exception e) {
            ;
        }
    }

    public MonitorModel findMonitorModelByQuotationsNumber(long qNumber) {
        for (MonitorModel m : monitorModels)
            if (m.getUniqNumberOfQuotations() == qNumber)
                return m;
        return null;
    }

    private void updateSettingsModelsData() {
        for (ChildModel m : settingsModels) {
            SettingsModel sm = (SettingsModel) m;
            sm.updateQuotationsList();
            sm.updateIndicators();
        }

        for (IndicatorBuilderModel m : builderModels) {
            m.updateQuotationsList();
        }
    }

    public void deleteSettingsModel(int id) {
        settingsModels.remove(id);
        int i = 0;
        for (ChildModel m : settingsModels) {
            m.setId(i++);
        }
        send(MLSubscriber.UPDATE_SYSTEMS);
    }

    public void deleteIndicatorBuilderModel(int id) {

    }

    public void addSystem(String nameOfSystem) {
        settingsModels.add(new SettingsModel(nameOfSystem, settingsModels.size(), this));
        send(MLSubscriber.ADD_SYSTEM);
    }

    public QuotationsModel getQuotationsModelByName(String name) {
        for (ChildModel m : quotationsModels) {
            if ((m.toString().equals(name)))
                return (QuotationsModel) m;
        }
        return null;
    }

    public void deleteQuotationsModel(int id) {
        quotationsModels.remove(id);
        int i = 0;
        for (ChildModel m : quotationsModels) {
            m.setId(i++);
        }
        send(MLSubscriber.UPDATE_QUOTATIONS);
        updateSettingsModelsData();
    }

    public void load() {
        send(MLSubscriber.LOAD);
    }

    public void save() {
        send(MLSubscriber.SAVE);
    }

    public void newM() {
        uniqQuotationsCounter = 0;
        uniqSystemSettingsCounter = 0;
        quotationsModels.clear();
        builderModels.clear();
        settingsModels.clear();
        quotationsModels.add(new QuotationsModel());
        builderModels.add(new IndicatorBuilderModel("Default", builderModels.size(), this));
        settingsModels.add(new SettingsModel("Default", settingsModels.size(), this));
        analyzerModel = new AnalyzerModel(this);
        send(MLSubscriber.NEW);
    }

    public long getUniqQuotationsNumber() {
        return uniqQuotationsCounter++;
    }

    public long getUniqSystemSettingsNumber() {
        return uniqSystemSettingsCounter++;
    }

    public QuotationsModel getQuotationsByUniqNumber(long number) {
        for (ChildModel m : quotationsModels) {
            QuotationsModel qm = (QuotationsModel) m;
            if (qm.getUniqNumber() == number)
                return qm;
        }
        return null;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeLong(uniqQuotationsCounter);
        out.writeLong(uniqSystemSettingsCounter);
        out.writeInt(quotationsModels.size());
        for (ChildModel m : quotationsModels) {
            QuotationsModel qm = (QuotationsModel) m;
            out.writeObject(qm);
        }

        out.writeInt(builderModels.size());
        for (IndicatorBuilderModel m : builderModels) {
            out.writeObject(m);
            out.writeInt(m.quotationsReference.value.id);
        }
        out.writeInt(settingsModels.size());
        for (ChildModel m : settingsModels) {
            SettingsModel sm = (SettingsModel) m;
            out.writeObject(sm);
            out.writeInt(sm.quotationsReference.value.id);
        }
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        uniqQuotationsCounter = in.readLong();
        uniqSystemSettingsCounter = in.readLong();
        int size = in.readInt();
        quotationsModels.clear();
        for (int i = 0; i < size; i++) {
            QuotationsModel m = (QuotationsModel) in.readObject();
            m.setMainModel(this);
            quotationsModels.add(m);
        }

        size = in.readInt();
        builderModels.clear();
        for (int i = 0; i < size; i++) {
            IndicatorBuilderModel m = (IndicatorBuilderModel) in.readObject();
            m.setMainModel(this);
            int id = in.readInt();
            m.quotationsReference.value = this.quotationsModels.get(id);
            builderModels.add(m);
        }
        size = in.readInt();
        settingsModels.clear();
        for (int i = 0; i < size; i++) {
            SettingsModel m = (SettingsModel) in.readObject();
            m.setMainModel(this);
            int id = in.readInt();
            m.quotationsReference.value = this.quotationsModels.get(id);
            settingsModels.add(m);
        }
    }

    public void startEmulator() {
        send(MLSubscriber.START_EMULATOR);
    }
}
