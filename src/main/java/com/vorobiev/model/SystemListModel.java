package com.vorobiev.model;

import com.vorobiev.entity.SystemInfo;
import com.vorobiev.ui.view.MLSubscriber;

public class SystemListModel extends Model {
    public final SystemInfo info;
    private long uniqNumberOfSystem;

    public SystemListModel(long uniqNumberOfSystem, SystemInfo info) {
        this.info = info;
        this.uniqNumberOfSystem = uniqNumberOfSystem;
    }

    public int getPercentProfit(int number) {
        if (info == null)
            return 0;
        if (info.getPercentProfitMap().size() < number)
            return 0;
        return info.getPercentProfitMap().get(number - 1);
    }

    public String getStringUniqNumberOfSystem() {
        return String.valueOf(uniqNumberOfSystem);
    }

    public long getUniqNumberOfSystem() {
        return uniqNumberOfSystem;
    }

    public void updateGraphic() {
        send(MLSubscriber.UPDATE_SYSTEM_MONITOR_GRAPHIC);
    }
}
