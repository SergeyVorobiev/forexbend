package com.vorobiev.ml.draft;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import util.PlotUtil;

import java.io.File;
import java.io.IOException;

// RecordReaders - parsing a specific vector from a file format and converting the data values into NDArray.
// DataSetIterator - take the produced NDArray and create mini-batches of NDArrays for training from it.
public class FirstModel {
    public static void init() {
        Nd4j.ENFORCE_NUMERICAL_STABILITY = true;
        int batchSize = 50;
        int seed = 123;
        double learningRate = 0.005;
        int nEpoch = 30;
        int numOutputs = 2;
        int numInputs = 2;
        int numHiddenNodes = 20;

        // Load the training data.
        DataSetIterator trainIter = loadCSVData("sdf", batchSize, 0, 2);

        // Load the test/evaluation data.
        DataSetIterator testIter = loadCSVData("sdf", batchSize, 0, 2);

        MultiLayerNetwork model = new MultiLayerNetwork(getConfiguration(seed, 1, learningRate, numInputs, numOutputs, numHiddenNodes));
        model.init();
        model.setListeners(new ScoreIterationListener(10));

        for (int i = 0; i < nEpoch; i++) {
            model.fit(trainIter);
        }

        Evaluation eval = new Evaluation(numOutputs);
        while (testIter.hasNext()) {
            DataSet t = testIter.next();
            INDArray features = t.getFeatureMatrix();
            INDArray labels = t.getLabels();
            INDArray predicted = model.output(features, false);
            eval.eval(labels, predicted);
        }

        System.out.println(eval.stats());
    }

    private static DataSetIterator loadCSVData(String pathName, int batchSize, int labelIndex, int numPossibleLabels) {
        RecordReader rr = new CSVRecordReader();
        try {
            rr.initialize(new FileSplit(new File(pathName)));
        } catch (Exception e) {
            e.printStackTrace();
            try {
                rr.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        return new RecordReaderDataSetIterator(rr, batchSize, labelIndex, numPossibleLabels);
    }

    private static MultiLayerConfiguration getConfiguration(int seed, int iterations, double learningRate, int numInputs, int numOutputs, int numHiddenNodes) {
        return new NeuralNetConfiguration.Builder()
                .seed(seed)
                .iterations(iterations)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .learningRate(learningRate)
                .updater(new Nesterovs(0.9))
                .list()
                .layer(0, new DenseLayer.Builder().nIn(numInputs)
                        .nOut(numHiddenNodes)
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.RELU)
                        .build())
                .layer(1, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SOFTMAX)
                        .nIn(numHiddenNodes).nOut(numOutputs).build()).pretrain(false).backprop(true).build();
    }

    private static void plot(MultiLayerNetwork model) throws IOException, InterruptedException {
        double xMin = -15;
        double xMax = 15;
        double yMin = -15;
        double yMax = 15;

        int nPointsPerAxis = 100;
        double[][] evalPoints = new double[nPointsPerAxis * nPointsPerAxis][2];
        int count = 0;
        for (int i = 0; i < nPointsPerAxis; i++) {
            for (int j = 0; j < nPointsPerAxis; j++) {
                double x = i * (xMax - xMin) / (nPointsPerAxis - 1) + xMin;
                double y = j * (yMax - yMin) / (nPointsPerAxis - 1) + yMin;
                evalPoints[count][0] = x;
                evalPoints[count][1] = y;
                count++;
            }
        }
        INDArray allXYPoints = Nd4j.create(evalPoints);
        INDArray predictionsAtXYPoints = model.output(allXYPoints);

        // Get all of the training data in a single array, and plot it.
        RecordReader rr = new CSVRecordReader();
        rr.initialize(new FileSplit(new File("someFile")));
        rr.reset();
        int nTrainPoints = 500;
        RecordReaderDataSetIterator trainIter = new RecordReaderDataSetIterator(rr, nTrainPoints, 0, 2);
        DataSet ds = trainIter.next();
        PlotUtil.plotTrainingData(ds.getFeatures(), ds.getLabels(), allXYPoints, predictionsAtXYPoints, nPointsPerAxis);

        // Get test data, run the test data through the network to generate predictions, and plot those predictions.
        RecordReader rrTest = new CSVRecordReader();
        rrTest.initialize(new FileSplit(new File("someFile")));
        rrTest.reset();
        int nTestPoints = 100;
        RecordReaderDataSetIterator testIter = new RecordReaderDataSetIterator(rrTest, nTestPoints, 0, 2);
        ds = testIter.next();
        INDArray testPredicted = model.output(ds.getFeatures());
        PlotUtil.plotTestData(ds.getFeatures(), ds.getLabels(), testPredicted, allXYPoints, predictionsAtXYPoints, nPointsPerAxis);
    }
}
