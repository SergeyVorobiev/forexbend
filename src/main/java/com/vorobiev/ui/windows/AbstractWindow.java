package com.vorobiev.ui.windows;

import com.vorobiev.model.Model;
import com.vorobiev.ui.view.Subscriber;
import com.vorobiev.ui.helpers.FXWindow;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;

import java.util.TreeMap;

public abstract class AbstractWindow implements Subscriber {
    protected Stage stage;
    protected FXMLLoader loader;
    protected String title;
    protected boolean resizable;
    protected Parent parent;
    protected String pathToFXMLLayout;
    protected final TreeMap<Integer, Runnable> signals = new TreeMap();

    public AbstractWindow(Stage stage, Model model, String title, String pathToFXMLLayout, boolean resizable) {
        this.title = title;
        this.resizable = resizable;
        this.stage = stage;
        this.pathToFXMLLayout = pathToFXMLLayout;
        model.subscribers.add(this);
    }

    public <T> T getView(String selector) {
        return (T) parent.lookup("#" + selector);
    }

    public void show() {
        Platform.runLater(() -> {
            stage.setOnCloseRequest((e) -> stage.close());
            loader = (FXMLLoader) FXWindow.showWindow(stage, pathToFXMLLayout);
            parent = loader.getRoot();
            stage.setTitle(title);
            stage.setResizable(resizable);
            initSignals();
            init();
        });
    }

    protected abstract void initSignals();

    @Override
    public void update(int signal) {
        Runnable run = signals.get(signal);
        if (run != null)
            run.run();
    }

    protected abstract void init();
}
