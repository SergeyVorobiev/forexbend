package com.vorobiev.ui.windows;

import com.vorobiev.emulator.QuotesGetter;
import com.vorobiev.emulator.QuotesSupplier;
import com.vorobiev.model.*;
import com.vorobiev.ui.controllers.MainWindowController;
import com.vorobiev.ui.helpers.FXWindow;
import com.vorobiev.ui.view.*;
import javafx.collections.ObservableList;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Accordion;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.TreeSet;

public class MainWindow extends AbstractWindow {

    private MainWindowController controller;
    private BarsView barsView;
    private BarsView predictedBarsView;
    private MainModel mainModel;
    private Accordion quotationsAccordion;
    private Accordion systemSettingsAccordion;
    private Accordion monitorAccordion;
    private ArrayList<QuotationsView> quotationsViews;
    private ArrayList<SettingsView> settingsViews;
    private ArrayList<MonitorView> monitorViews;

    public MainWindow(Stage stage, MainModel mainModel, String title, boolean resizable) {
        super(stage, mainModel, title, "fx/mainLayout.fxml", resizable);
        this.mainModel = mainModel;
    }

    private void load() {
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream("D:\\hhh"));
            mainModel = (MainModel) ois.readObject();
            mainModel.subscribers.add(this);
            controller.setMainModel(mainModel);
            loadMainElements();
            ois.close();
        } catch (Exception e) {
            System.out.println("Can not load model from file with path.");
            e.printStackTrace();
        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (Exception e2) {
                    System.out.println("Can not close input stream after load the model.");
                    e2.printStackTrace();
                }
            }
        }
    }

    private void save() {
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream("D:\\hhh"));
            oos.writeObject(mainModel);
            oos.close();
        } catch (Exception e) {
            System.out.println("Can not save the model.");
            e.printStackTrace();
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (Exception e2) {
                    System.out.println("Can not close output stream after save the model.");
                    e2.printStackTrace();
                }
            }
        }
    }

    private void loadMainElements() {
        loadBarVisualizer();
        loadQuotations();
        loadSettings();
        loadIndicatorBuilder();
        loadMonitor();
        loadAnalyzer();
    }

    private void loadAnalyzer() {
        new AnalyzerView(this.parent, new AnalyzerModel(this.mainModel));
    }

    private void loadMonitor() {
        monitorViews = new ArrayList();
        monitorAccordion = getView("monitorViews");
        ObservableList list = monitorAccordion.getPanes();
        list.clear();
        for (MonitorModel m : mainModel.monitorModels) {
            MonitorView monitorView = new MonitorView("fx/monitorLayout.fxml", m);
            monitorViews.add(monitorView);
            list.add(monitorView.getView());
        }
    }

    @Override
    protected void init() {
        stage.centerOnScreen();
        stage.getIcons().add(new Image(FXWindow.pathURL + "fx/iconML3.png"));
        controller = this.loader.getController();
        controller.setMainModel(mainModel);
        mainModel.newM();
        // startEmulator();
    }

    public void startEmulator() {
        Thread thread = new Thread(new QuotesGetter(mainModel));
        thread.setDaemon(true);
        thread.start();
        Thread thread2 = new Thread(new QuotesSupplier());
        thread2.setDaemon(true);
        thread2.start();
    }

    private void loadBarVisualizer() {
        Canvas canvas = getView("barCanvas");
        barsView = new BarsView(new TreeSet());
        mainModel.setBarsView(barsView);
        barsView.draw(canvas, 0);
        Canvas predictedCanvas = getView("barPredictedCanvas");
        predictedBarsView = new BarsView(new TreeSet());
        mainModel.setPredictedBarsView(predictedBarsView);
        predictedBarsView.draw(predictedCanvas, 0);
    }

    private void loadQuotations() {
        quotationsAccordion = getView("quotationsAccordion");
        quotationsViews = new ArrayList();
        ObservableList list = quotationsAccordion.getPanes();
        list.clear();
        for (int i = 0; i < mainModel.quotationsModels.size(); i++) {
            QuotationsModel m = (QuotationsModel) mainModel.quotationsModels.get(i);
            QuotationsView quotationsView = new QuotationsView("fx/quotationsLayout.fxml", m);
            quotationsViews.add(quotationsView);
            if (i != 0)
                list.add(quotationsView.getView());
        }
    }

    private void addSystem() {
        ObservableList list = systemSettingsAccordion.getPanes();
        int id = mainModel.settingsModels.size() - 1;
        SettingsView settingsView = new SettingsView("fx/systemOutLayout.fxml", (SettingsModel) mainModel.settingsModels.get(id));
        settingsViews.add(settingsView);
        list.add(settingsView.getView());
    }

    private void addQuotations() {
        ObservableList list = quotationsAccordion.getPanes();
        int id = mainModel.quotationsModels.size() - 1;
        QuotationsView quotationsView = new QuotationsView("fx/quotationsLayout.fxml", (QuotationsModel) mainModel.quotationsModels.get(id));
        quotationsViews.add(quotationsView);
        list.add(quotationsView.getView());
        list = monitorAccordion.getPanes();
        MonitorView mv = new MonitorView("fx/monitorLayout.fxml", mainModel.monitorModels.get(mainModel.monitorModels.size() - 1));
        monitorViews.add(mv);
        list.add(mv.getView());
    }

    private void loadIndicatorBuilder() {
        Accordion indicatorBuilder = getView("indicatorBuilder");
        ObservableList list = indicatorBuilder.getPanes();
        list.clear();
        for (IndicatorBuilderModel m : mainModel.builderModels) {
            IndicatorBuilderView builderView = new IndicatorBuilderView("fx/indicatorBuilderLayout.fxml", m);
            list.add(builderView.getView());
        }
    }

    private void loadSettings() {
        settingsViews = new ArrayList();
        systemSettingsAccordion = getView("systemSettings");
        ObservableList list = systemSettingsAccordion.getPanes();
        list.clear();
        for (ChildModel m : mainModel.settingsModels) {
            SettingsView settingsView = new SettingsView("fx/systemOutLayout.fxml", (SettingsModel) m);
            settingsViews.add(settingsView);
            list.add(settingsView.getView());
        }
    }

    private void updateQuotations() {
        ObservableList list = quotationsAccordion.getPanes();
        int i = 0;
        for (QuotationsView view : quotationsViews) {
            ChildModel m = null;
            try {
                m = mainModel.quotationsModels.get(i++);
            } catch (Exception e) {
                ;
            }
            if (m == null || m != view.model) {
                list.remove(quotationsViews.remove(i - 1).getView());
                break;
            }
        }
    }

    private void updateSystems() {
        ObservableList list = systemSettingsAccordion.getPanes();
        int i = 0;
        for (SettingsView view : settingsViews) {
            ChildModel m = null;
            try {
                m = mainModel.settingsModels.get(i++);
            } catch (Exception e) {
                ;
            }
            if (m == null || m != view.model) {
                list.remove(settingsViews.remove(i - 1).getView());
                break;
            }
        }
    }

    @Override
    public void initSignals() {
        signals.put(MLSubscriber.ADD_QUOTATIONS, this::addQuotations);
        signals.put(MLSubscriber.UPDATE_QUOTATIONS, this::updateQuotations);
        signals.put(MLSubscriber.UPDATE_SYSTEMS, this::updateSystems);
        signals.put(MLSubscriber.LOAD, this::load);
        signals.put(MLSubscriber.SAVE, this::save);
        signals.put(MLSubscriber.NEW, this::loadMainElements);
        signals.put(MLSubscriber.ADD_SYSTEM, this::addSystem);
        signals.put(MLSubscriber.START_EMULATOR, this::startEmulator);
    }
}
