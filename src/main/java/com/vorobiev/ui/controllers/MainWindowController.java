package com.vorobiev.ui.controllers;

import com.vorobiev.model.MainModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class MainWindowController {
    @FXML
    private Canvas barCanvas;

    @FXML
    private Canvas barPredictedCanvas;

    @FXML
    private TextField nameOfSystem;

    private MainModel mainModel;
    private int startTouchBarsViewX;
    private int startTouchBarsPredictedViewX;

    @FXML
    void barCanvasMouseDragged(MouseEvent event) {
        if (mainModel.getBarsView() != null)
            mainModel.getBarsView().draw(barCanvas, (int) event.getX() - startTouchBarsViewX);
        startTouchBarsViewX = (int) event.getX();
    }

    @FXML
    void addSystemAction(ActionEvent event) {
        mainModel.addSystem(nameOfSystem.getText());
    }


    @FXML
    void saveAction(ActionEvent event) {
        mainModel.save();
    }

    @FXML
    void loadAction(ActionEvent event) {
        mainModel.load();
    }

    @FXML
    void newAction(ActionEvent event) {
        mainModel.newM();
    }

    public void setMainModel(MainModel mainModel) {
        this.mainModel = mainModel;
    }

    @FXML
    void barCanvasMousePressed(MouseEvent event) {
        startTouchBarsViewX = (int) event.getX();
    }

    @FXML
    void barPredictedCanvasMouseDragged(MouseEvent event) {
        if (mainModel.getPredictedBarsView() != null)
            mainModel.getPredictedBarsView().draw(barPredictedCanvas, (int) event.getX() - startTouchBarsPredictedViewX);
        startTouchBarsPredictedViewX = (int) event.getX();
    }

    @FXML
    void addQuotations(ActionEvent event) {
        mainModel.addQuotations("EURUSD1440.csv");
    }

    @FXML
    void testAnalyzer(ActionEvent event) {
        mainModel.analyzerModel.test();
    }

    @FXML
    void startTest(ActionEvent event) {
        mainModel.startEmulator();
    }

    @FXML
    void barPredictedCanvasMousePressed(MouseEvent event) {
        startTouchBarsPredictedViewX = (int) event.getX();
    }
}
