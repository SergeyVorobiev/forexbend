package com.vorobiev.ui.helpers;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;

import java.util.Map;
import java.util.TreeMap;

public class BarChartHelpers {
    public static void fillIntegerBarChart(BarChart bc, String title, TreeMap<String, Integer> bars) {
        //final CategoryAxis xAxis = (CategoryAxis) bc.getXAxis();
        //final NumberAxis yAxis = (NumberAxis) bc.getYAxis();
        bc.setTitle(title);
        // xAxis.setLabel("Country");
        //yAxis.setLabel("Percent");

        XYChart.Series series = new XYChart.Series();
        for (Map.Entry<String, Integer> entry : bars.entrySet())
            series.getData().add(new XYChart.Data(entry.getKey(), entry.getValue()));
        bc.getData().clear();
        bc.getData().add(series);
    }
}
