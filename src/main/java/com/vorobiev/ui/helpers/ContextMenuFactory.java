package com.vorobiev.ui.helpers;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

public class ContextMenuFactory {
    public static ContextMenu getMenuWithOneOption(String optionName, Runnable action) {
        ContextMenu menu = new ContextMenu();
        MenuItem item = new MenuItem(optionName);
        item.setOnAction((e) -> action.run());
        menu.getItems().add(item);
        return menu;
    }
}
