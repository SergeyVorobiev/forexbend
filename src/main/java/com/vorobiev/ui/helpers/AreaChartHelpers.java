package com.vorobiev.ui.helpers;

import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.util.function.Function;

public class AreaChartHelpers {
    public static void fillOneIntegerSeriesAreaChart(AreaChart ac, String title, String seriesName, Function<Integer, Integer> yDataGetter) {
        ac.setTitle(title);
        XYChart.Series series = new XYChart.Series();
        series.setName(seriesName);
        NumberAxis xAxis = (NumberAxis) ac.getXAxis();
        for (int i = (int) xAxis.getLowerBound(); i <= (int) xAxis.getUpperBound(); i += xAxis.getTickUnit())
            series.getData().add(new XYChart.Data(i, yDataGetter.apply(i)));
        ac.getData().clear();
        ac.getData().add(series);
    }
}
