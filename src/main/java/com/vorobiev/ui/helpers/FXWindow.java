package com.vorobiev.ui.helpers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class FXWindow {

    public static final String pathURL = "file:\\" + System.getProperty("user.dir") + "\\src\\main\\resources\\";
    public static final String path = System.getProperty("user.dir") + "\\src\\main\\resources\\";

    public static Parent getLayout(String resource) {
        try {
            return (Parent) getLoader(resource).load();
        } catch (IOException e) {
            System.out.println("Can't create layout by path" + pathURL + resource);
        }
        return null;
    }

    public static Parent getLayout(FXMLLoader loader) {
        try {
            return (Parent) loader.load();
        } catch (IOException e) {
            System.out.println("Can't create layout by path with loader.");
        }
        return null;
    }

    public static FXMLLoader getLoader(String resource) {
        try {
            return new FXMLLoader(new URL(pathURL + resource));
        } catch (MalformedURLException e) {
            System.out.println("Can't create url by path " + pathURL + resource);
        }
        return null;
    }

    public static Object showWindow(Stage stage, String resource) {
        FXMLLoader loader = null;
        try {
            loader = new FXMLLoader(new URL(pathURL + resource));
            Parent windowLayout = loader.load();
            createScene(stage, windowLayout);
        } catch (IOException e) {
            System.out.println("Cannot show " + pathURL + resource + "\n" + e.getMessage());
            stage.close();
        }
        return loader;
    }

    private static void createScene(Stage stage, Parent parent) {
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.setOnCloseRequest((e) -> stage.close());
        stage.show();
    }

    public static void showWindow(Stage stage, Parent parent) {
        createScene(stage, parent);
    }

    public static void printTreeView(Parent p) {
        printTreeViewRecur(p, "|-");
    }

    private static void printTreeViewRecur(Parent p, String s) {
        System.out.println(s + p);
        for (Node n : p.getChildrenUnmodifiable()) {
            if (n instanceof Parent) {
                printTreeViewRecur((Parent) n, s + "-");
            }
        }
    }
}
