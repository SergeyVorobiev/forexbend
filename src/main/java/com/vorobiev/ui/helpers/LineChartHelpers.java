package com.vorobiev.ui.helpers;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.util.function.Function;

public class LineChartHelpers {
    public static void fillOneIntegerSeriesLineChart(LineChart lc, String title, String seriesName, Function<Integer, Integer> yDataGetter) {
        lc.setTitle(title);
        XYChart.Series series = new XYChart.Series();
        series.setName(seriesName);
        NumberAxis xAxis = (NumberAxis) lc.getXAxis();
        for (int i = (int) xAxis.getLowerBound(); i <= (int) xAxis.getUpperBound(); i += xAxis.getTickUnit())
            series.getData().add(new XYChart.Data(i, yDataGetter.apply(i)));
        lc.getData().clear();
        lc.getData().add(series);
    }
}
