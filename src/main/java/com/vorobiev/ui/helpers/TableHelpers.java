package com.vorobiev.ui.helpers;

import com.vorobiev.entity.Bar;
import com.vorobiev.entity.OrderResult;
import com.vorobiev.entity.Score;
import com.vorobiev.entity.SystemInfo;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class TableHelpers {
    public static void createColumnsForInfoAboutSystemTable(TableView<ObservableList<String>> tv) {
        ObservableList<TableColumn<ObservableList<String>, ?>> columnList = tv.getColumns();
        int index = 0;
        columnList.add(getColumn("Параметр", index++));
        columnList.add(getColumn("Значение", index));
    }

    public static void createColumnsForSystemReportTable(TableView<ObservableList<String>> tv) {
        ObservableList<TableColumn<ObservableList<String>, ?>> columnList = tv.getColumns();
        int index = 0;
        columnList.add(getColumn("Дата открытия", index++));
        columnList.add(getColumn("Курс", index++));
        columnList.add(getColumn("Дата закрытия", index++));
        columnList.add(getColumn("Курс", index++));
        columnList.add(getColumn("Минимум", index++));
        columnList.add(getColumn("Тики", index++));
        columnList.add(getColumn("Ставка", index++));
        columnList.add(getColumn("Результат", index++));
        columnList.add(getColumn("Капитал", index));
    }

    public static void fillTicksAndIndicatorsOfBar(TableView<ObservableList<String>> tv, ArrayList<Bar> bars) {
        ObservableList<ObservableList<String>> data = FXCollections.observableArrayList();
        Set<String> predictableNames = bars.isEmpty() ? new LinkedHashSet() : bars.get(0).getPredictableIndicatorNames();
        Set<String> arithmeticNames = bars.isEmpty() ? new LinkedHashSet() : bars.get(0).getArithmeticIndicatorNames();
        for (Bar b : bars) {
            ObservableList<String> row = FXCollections.observableArrayList();
            row.add(b.date.toString());
            row.add(String.valueOf(b.open));
            row.add(String.valueOf(b.max));
            row.add(String.valueOf(b.min));
            row.add(String.valueOf(b.close));
            row.add(String.valueOf(b.volume));
            for (String name : arithmeticNames) {
                row.add(String.valueOf(b.getArithmeticIndicator(name)));
            }
            for (String name : predictableNames) {
                row.add(String.valueOf(b.getPredictableIndicator(name)));
            }
            data.add(row);
        }
        if (!bars.isEmpty())
            tv.setItems(data);
        ArrayList<String> columnNames = new ArrayList();
        columnNames.add("Date");
        columnNames.add("Open");
        columnNames.add("Max");
        columnNames.add("Min");
        columnNames.add("Close");
        columnNames.add("Volume");
        columnNames.addAll(arithmeticNames);
        columnNames.addAll(predictableNames);
        ObservableList columnList = tv.getColumns();
        columnList.clear();
        int index = 0;
        for (String n : columnNames) {
            columnList.add(getColumn(n, index++));
        }
    }

    private static Score getScore(ArrayList<Score> scores, int i) {
        try {
            return scores.get(i);
        } catch (Exception e) {
            return null;
        }
    }

    public static void fillInfoAboutSystem(TableView<ObservableList<String>> tv, SystemInfo info) {
        ObservableList<ObservableList<String>> data = FXCollections.observableArrayList();
        if (info != null) {
            addBinaryRow("Оптимальное F", String.valueOf(info.getOptimalF()), data);
            addBinaryRow("TWR", String.valueOf(info.getTwr()), data);
            addBinaryRow("Счет Z", String.valueOf(info.getZ()), data);
            addBinaryRow("Коэф. лин. корреляции", String.valueOf(info.getCorrelation()), data);
            addBinaryRow("Среднее геометрическое", String.valueOf(info.getAverageGeom()), data);
            addBinaryRow("Общее число сделок", String.valueOf(info.getCountOrders()), data);
            addBinaryRow("Выйгрышей", String.valueOf(info.getProfitCount()), data);
            addBinaryRow("Проигрышей", String.valueOf(info.getLoseCount()), data);
            addBinaryRow("Процент выйгрышей", String.valueOf(info.getPercentProfit()), data);
            addBinaryRow("Макс. прибыль", String.valueOf(info.getMaxProfit()), data);
            addBinaryRow("Макс. убыток", String.valueOf(info.getMaxLose()), data);
            addBinaryRow("Сред. прибыль", String.valueOf(info.getAverProfit()), data);
            addBinaryRow("Сред. убыток", String.valueOf(info.getAverLose()), data);
            addBinaryRow("Мат. ожидание", String.valueOf(info.getMathExpect()), data);
            addBinaryRow("Средняя геометрич. сделка", String.valueOf(info.getAverageGeomOrder()), data);
            addBinaryRow("Макс. проседание", String.valueOf(info.getMaxMin()), data);
        }
        tv.setItems(data);
    }

    public static void addBinaryRow(String firstColumn, String secondColumn, ObservableList<ObservableList<String>> data) {
        ObservableList<String> row = FXCollections.observableArrayList();
        row.add(firstColumn);
        row.add(secondColumn);
        data.add(row);
    }

    public static void fillSystemResult(TableView<ObservableList<String>> tv, Collection<OrderResult> results, ArrayList<Score> scores) {
        ObservableList<ObservableList<String>> data = FXCollections.observableArrayList();
        int index = 0;
        for (OrderResult result : results) {
            ObservableList<String> row = FXCollections.observableArrayList();
            row.add(result.getOpenDate().toString());
            row.add(String.valueOf(result.getOpen()));
            row.add(result.getCloseDate() == null ? "" : result.getCloseDate().toString());
            row.add(String.valueOf(result.getClose()));
            row.add(String.valueOf(result.getMin()));
            row.add(String.valueOf(result.getTicks()));
            Score score = getScore(scores, index++);
            row.add(score == null ? "0" : String.valueOf(score.bet));
            row.add(score == null ? "0" : String.valueOf(score.earnings));
            row.add(score == null ? "0" : String.valueOf(score.balance));
            data.add(row);
        }
        tv.setItems(data);
    }

    private static TableColumn<ObservableList<String>, String> getColumn(String columnName, int index) {
        TableColumn<ObservableList<String>, String> col = new TableColumn(columnName);
        col.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().get(index)));
        return col;
    }
}
