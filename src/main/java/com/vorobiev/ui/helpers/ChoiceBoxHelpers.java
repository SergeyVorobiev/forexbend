package com.vorobiev.ui.helpers;

import com.vorobiev.helpers.Container;
import com.vorobiev.model.ChildModel;
import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class ChoiceBoxHelpers {
    public static ChoiceBox initChoiceBox(ChoiceBox cb, ArrayList<String> options, Container<String> option, Runnable func) {
        return addListener(updateChoiceBox(cb, options, option), options, option, func);
    }

    private static <T> ChoiceBox addListener(ChoiceBox cb, ArrayList<T> options, Container<T> option, Runnable func) {
        cb.getSelectionModel().selectedIndexProperty().addListener((ov, value, nValue) -> {
            if (nValue.intValue() != -1) {
                option.value = options.get(nValue.intValue());
                if (func != null)
                    func.run();
            }
        });
        return cb;
    }

    public static <T extends ChildModel> ChoiceBox initChoiceModelBox(ChoiceBox cb, ArrayList<T> options, Container<T> option, Runnable func) {

        return addListener(updateChoiceModelBox(cb, options, option), options, option, func);
    }

    public static ChoiceBox initChoiceBox(ChoiceBox cb, ArrayList<String> options, Container<String> option) {
        return initChoiceBox(cb, options, option, null);
    }

    public static <T extends ChildModel> ChoiceBox updateChoiceModelBox(ChoiceBox cb, ArrayList<T> options, Container<T> option) {
        cb.setItems(FXCollections.observableArrayList(options.stream().map(m -> m.toString()).collect(Collectors.toList())));
        cb.setValue(option.value.toString());
        return cb;
    }

    public static ChoiceBox updateChoiceBox(ChoiceBox cb, ArrayList<String> options, Container<String> option) {
        cb.setItems(FXCollections.observableArrayList(options));
        cb.setValue(option.value);
        return cb;
    }
}
