package com.vorobiev.ui.view;

import com.vorobiev.model.IndicatorBuilderModel;
import com.vorobiev.ui.helpers.ChoiceBoxHelpers;
import javafx.scene.Node;
import javafx.scene.control.*;

public class IndicatorBuilderView extends View {
    private final TitledPane view;
    private IndicatorBuilderModel model;
    private ChoiceBox quotationsBox;

    public IndicatorBuilderView(String pathToLayout, IndicatorBuilderModel model) {
        super(pathToLayout, model);
        this.model = model;
        view = new TitledPane();
        view.setText(model.toString());
        view.setId(String.valueOf(model.getId()));
        view.setContent(layout);
        TextArea indicatorFormula = getElement("indicatorFormula");
        indicatorFormula.setText(model.getIndicatorFormula());
        TextField indicatorName = getElement("indicatorName");
        indicatorName.setText(model.getIndicatorName());
        Button computeIndicator = getElement("computeIndicator");
        quotationsBox = getElement("quotationsChooser");
        ChoiceBoxHelpers.initChoiceModelBox(quotationsBox, model.getMainModel().quotationsModels, model.quotationsReference, null);
        computeIndicator.setOnAction((actionEvent) ->
                model.computePredictableIndicator(indicatorName.getText(), indicatorFormula.getText(), quotationsBox.getSelectionModel().getSelectedIndex()));
    }

    private void updateQuotationsList() {
        ChoiceBoxHelpers.updateChoiceModelBox(quotationsBox, model.getMainModel().quotationsModels, model.quotationsReference);
    }

    @Override
    public void initSignals() {
        signals.put(MLSubscriber.UPDATE_INDICATORS_QUOTATIONS_LIST, this::updateQuotationsList);
    }

    @Override
    public Node getView() {
        return view;
    }
}
