package com.vorobiev.ui.view;

import com.vorobiev.model.QuotationsModel;
import com.vorobiev.ui.helpers.ContextMenuFactory;
import com.vorobiev.ui.helpers.TableHelpers;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class QuotationsView extends View {
    public final QuotationsModel model;
    private TableView tableView;
    private final TitledPane view;

    public QuotationsView(QuotationsModel model) {
        super("", model);
        this.model = model;
        view = null;
    }

    public QuotationsView(String pathToLayout, QuotationsModel model) {
        super(pathToLayout, model);
        this.model = model;
        tableView = getElement("quotationsTable");
        ContextMenu menuDelete = ContextMenuFactory.getMenuWithOneOption("delete", this::deleteCurrentQuotations);
        view = new TitledPane();
        view.addEventHandler(MouseEvent.MOUSE_CLICKED, (EventHandler<MouseEvent>) event -> {
            if (event.getButton() == MouseButton.SECONDARY)
                menuDelete.show(view, event.getScreenX(), event.getScreenY());
        });
        view.setText(model.toString());
        view.setId(String.valueOf(model.getId()));
        view.setContent(layout);
        updateQuotationsTable();
    }

    private void deleteCurrentQuotations() {
        model.getMainModel().deleteQuotationsModel(model.getId());
    }

    @Override
    public void initSignals() {
        signals.put(MLSubscriber.UPDATE_QUOTATIONS_TABLE, this::updateQuotationsTable);
        signals.put(MLSubscriber.UPDATE_TITLE, this::updateTitle);
    }

    private void updateTitle() {
        view.setText(model.toString());
    }

    private void updateQuotationsTable() {
        TableHelpers.fillTicksAndIndicatorsOfBar(tableView, model.getIndicatorBars());
    }

    @Override
    public Node getView() {
        return view;
    }
}
