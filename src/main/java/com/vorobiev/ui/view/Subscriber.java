package com.vorobiev.ui.view;

public interface Subscriber {
    void update(int signal);
}
