package com.vorobiev.ui.view;

import com.vorobiev.entity.Bar;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.TreeSet;

public class BarsView {
    private TreeSet<Bar> bars;
    private ArrayList<Bar> drawingBars = new ArrayList();
    private int leftLineMargin = 60;
    private int curStartXPosition = leftLineMargin;
    private int curEndXPosition = 0;
    private int spaceBetweenBars = 1;
    private int maxLength = 0;
    private int[] minMax = new int[2];
    private int heightText = 10;

    public BarsView(TreeSet<Bar> bars) {
        this.bars = bars;
        maxLength = (bars.size() - 1) * spaceBetweenBars + bars.size() * Bar.width;
    }

    public void setBars(TreeSet<Bar> bars) {
        this.bars = bars;
        maxLength = (bars.size() - 1) * spaceBetweenBars + bars.size() * Bar.width;
    }

    public void draw(Canvas canvas, int offsetX) {
        drawGrid(canvas);
        drawBars(canvas, offsetX);
    }

    private void drawGrid(Canvas canvas) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.strokeRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    private void drawBars(Canvas canvas, int offsetX) {
        GraphicsContext gc = canvas.getGraphicsContext2D();

        // Draw lines.
        gc.setFill(Color.GRAY);
        int heightTop = heightText + 1;
        int heightBottom = (int) canvas.getHeight() - 2;
        int heightCenter = (int) (canvas.getHeight() / 2) - heightText / 2;
        int lineLength = (int) canvas.getWidth() - leftLineMargin - 2;
        gc.fillRect(leftLineMargin, heightTop - heightText / 2, lineLength, 1);
        gc.fillRect(leftLineMargin, heightCenter - heightText / 2, lineLength, 1);
        gc.fillRect(leftLineMargin, heightBottom - heightText / 2, lineLength, 1);

        curStartXPosition -= offsetX;
        curEndXPosition = curStartXPosition + lineLength;

        int rightBorder = leftLineMargin + lineLength;

        // Determine drawing x-bounds.
        if (curStartXPosition < leftLineMargin) {
            curStartXPosition = leftLineMargin;
            curEndXPosition = curStartXPosition + lineLength;
        } else if (curEndXPosition > maxLength && maxLength > rightBorder) {
            curEndXPosition = maxLength;
            curStartXPosition = curEndXPosition - lineLength;
        }

        // Determine drawing bars according to x-bounds.
        int curBarXPosition = leftLineMargin;
        for (Bar b : bars) {
            if (curBarXPosition >= curStartXPosition && curBarXPosition + Bar.width <= curEndXPosition) {
                drawingBars.add(b);
            } else if (curBarXPosition + Bar.width > curEndXPosition) {
                break;
            }
            curBarXPosition += (Bar.width + spaceBetweenBars);
        }
        int distanceBetweenTopCanvasAndTopLabelLine = heightTop - heightText / 2;
        int distanceBetweenBottomCanvasAndBottomLabelLine = (int) canvas.getHeight() - (heightBottom - heightText / 2);
        int drawingBarsHeight = (int) canvas.getHeight() - (distanceBetweenTopCanvasAndTopLabelLine +
                distanceBetweenBottomCanvasAndBottomLabelLine);

        // Determine drawing y-bounds.
        getMinMax(drawingBars, minMax);
        float scaleFactor = (minMax[1] - minMax[0]) / (float) drawingBarsHeight;
        if (scaleFactor <= 0)
            scaleFactor = 1;

        // Draw bars.
        curBarXPosition = leftLineMargin;
        for (Bar b : drawingBars) {
            int heightBody = (int) (b.heightBody / scaleFactor);
            int heightPick = (int) (b.heightPick / scaleFactor);
            int maxBody = b.isUp ? b.close : b.open;
            int startDrawPickY = (int) ((minMax[1] - b.max) / scaleFactor) + distanceBetweenTopCanvasAndTopLabelLine + 1;
            int startDrawBodyY = (int) ((minMax[1] - maxBody) / scaleFactor) + distanceBetweenTopCanvasAndTopLabelLine + 1;
            if (heightBody < 1)
                heightBody = 1;
            if (b.isUp) {
                gc.setFill(Color.BLUE);
                gc.fillRect(curBarXPosition, startDrawBodyY, Bar.width, heightBody);
            } else {
                gc.setFill(Color.RED);
                gc.fillRect(curBarXPosition, startDrawBodyY, Bar.width, heightBody);
            }
            gc.fillRect(curBarXPosition + b.width / 2, startDrawPickY, 1, heightPick);
            curBarXPosition += (Bar.width + spaceBetweenBars);
        }

        // Draw labels.
        gc.setFill(Color.BLACK);
        int leftTextMargin = 5;
        String maxLabel = Float.toString(minMax[1] / 10000.0f);
        String minLabel = Float.toString(minMax[0] / 10000.0f);
        String centerLabel = Float.toString((minMax[1] + minMax[0]) / 20000.0f);
        gc.fillText(maxLabel, leftTextMargin, heightTop);
        gc.fillText(minLabel, leftTextMargin, heightBottom);
        gc.fillText(centerLabel, leftTextMargin, heightCenter);
        drawingBars.clear();
    }

    private void getMinMax(ArrayList<Bar> bars, int[] result) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (Bar b : bars) {
            if (b.min < min)
                min = b.min;
            if (b.max > max)
                max = b.max;
        }
        result[0] = min;
        result[1] = max;
    }
}
