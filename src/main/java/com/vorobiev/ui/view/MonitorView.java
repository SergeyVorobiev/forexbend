package com.vorobiev.ui.view;

import com.vorobiev.model.MonitorModel;
import com.vorobiev.model.SystemListModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;

import java.util.ArrayList;

public class MonitorView extends View {
    public final MonitorModel model;
    private TitledPane view;
    private ArrayList<SystemListView> listViews = new ArrayList();
    private ListView list;

    public MonitorView(String pathToLayout, MonitorModel model) {
        super(pathToLayout, model);
        this.model = model;
        view = new TitledPane();
        view.setText(model.toString());
        view.setId(String.valueOf(model.getId()));
        view.setContent(layout);
        list = getElement("systemsList");
        ObservableList<String> data = FXCollections.observableArrayList();
        for (SystemListModel m : model.systemListModels) {
            listViews.add(new SystemListView("fx/systemListLayout.fxml", m));
            data.add(m.getStringUniqNumberOfSystem());
        }
        list.setCellFactory((thisList) -> new SystemListCell());
        list.setItems(data);
    }

    private void removeOutdatedSystem() {
        for (SystemListView slv : listViews) {
            if (slv.model.info.uniqNumberOfQuotations != model.getUniqNumberOfQuotations())
                listViews.remove(slv);
            list.getItems().remove(slv.model.getStringUniqNumberOfSystem());
        }
    }

    private void addSystem() {
        listViews.add(new SystemListView("fx/systemListLayout.fxml", model.systemListModels.get(model.systemListModels.size() - 1)));
        list.getItems().add(listViews.get(listViews.size() - 1).model.getStringUniqNumberOfSystem());
    }

    private class SystemListCell extends ListCell<String> {
        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                for (SystemListView m : listViews) {
                    if (m.model.getStringUniqNumberOfSystem().equals(item)) {
                        setGraphic(m.getView());
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void initSignals() {
        signals.put(MLSubscriber.ADD_SYSTEM_MONITOR, this::addSystem);
        signals.put(MLSubscriber.REMOVE_OUTDATED_SYSTEM_MONITOR, this::removeOutdatedSystem);
    }

    @Override
    public Node getView() {
        return view;
    }
}
