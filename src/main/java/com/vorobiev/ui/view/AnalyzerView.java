package com.vorobiev.ui.view;

import com.vorobiev.model.AnalyzerModel;
import com.vorobiev.ui.helpers.ChoiceBoxHelpers;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;

public class AnalyzerView extends View {

    private Button analyze;
    private ChoiceBox daysBeforeAnalyzer;
    private ChoiceBox indicatorAnalyzer;
    private ChoiceBox systemAnalyzer;
    public final AnalyzerModel model;

    public AnalyzerView(Parent layout, AnalyzerModel model) {
        super(layout, model);
        this.model = model;
        daysBeforeAnalyzer = getElement("daysBeforeAnalyzer");
        systemAnalyzer = getElement("systemAnalyzer");
        indicatorAnalyzer = getElement("indicatorAnalyzer");
        analyze = getElement("analyze");
        ChoiceBoxHelpers.initChoiceBox(daysBeforeAnalyzer, model.daysBefore, model.daysBeforeReference);
        ChoiceBoxHelpers.initChoiceModelBox(systemAnalyzer, model.model.settingsModels, model.systemAnalyzerReference, null);
        ChoiceBoxHelpers.initChoiceBox(indicatorAnalyzer, model.indicatorAnalyzer, model.indicatorAnalyzerReference);
        analyze.setOnAction((e) -> model.analyze());
    }

    @Override
    protected void initSignals() {

    }

    @Override
    public Node getView() {
        return null;
    }
}
