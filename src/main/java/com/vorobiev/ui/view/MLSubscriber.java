package com.vorobiev.ui.view;

public interface MLSubscriber extends Subscriber {
    int UPDATE_QUOTATIONS_TABLE = 0;
    int UPDATE_SYSTEM_RESULT = 1;
    int UPDATE_INDICATORS_FOR_SYSTEMS = 2;
    int ADD_QUOTATIONS = 3;
    int UPDATE_SYSTEMS_QUOTATIONS_LIST = 4;
    int UPDATE_QUOTATIONS = 5;
    int UPDATE_TITLE = 6;
    int LOAD = 7;
    int SAVE = 8;
    int NEW = 9;
    int UPDATE_INDICATORS_QUOTATIONS_LIST = 10;
    int ADD_SYSTEM = 11;
    int UPDATE_SYSTEMS = 12;
    int UPDATE_SYSTEM_MONITOR_GRAPHIC = 13;
    int UPDATE_SYSTEM_RESULT_WITHOUT_F = 14;
    int ADD_SYSTEM_MONITOR = 15;
    int REMOVE_OUTDATED_SYSTEM_MONITOR = 16;
    int START_EMULATOR = 17;
}
