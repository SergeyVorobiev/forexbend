package com.vorobiev.ui.view;

import com.vorobiev.model.SettingsModel;
import com.vorobiev.ui.helpers.*;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class SettingsView extends View {

    private final TitledPane view;
    private TableView systemReportTable;
    private TableView systemInfoTable;
    private ChoiceBox openIndicatorBox;
    private ChoiceBox closeIndicatorBox;
    private ChoiceBox leversBox;
    private ChoiceBox callsBox;
    private ChoiceBox typesBox;
    private ChoiceBox profitsDaysBox;
    private ChoiceBox ordersDaysBox;
    private ChoiceBox quotationsBox;
    private AreaChart<Number, Number> optimalFGraphic;
    private BarChart daysPercentProfitGraphic;
    public final SettingsModel model;

    public SettingsView(String pathToLayout, SettingsModel model) {
        super(pathToLayout, model);
        this.model = model;
        view = new TitledPane();
        view.setText(model.toString());
        view.setId(String.valueOf(model.getId()));
        view.setContent(layout);
        ContextMenu menuDelete = ContextMenuFactory.getMenuWithOneOption("delete", this::deleteCurrentSettings);
        view.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.SECONDARY)
                menuDelete.show(view, event.getScreenX(), event.getScreenY());
        });
        Button run = getElement("runSystem");
        run.setOnAction((actionEvent) -> model.runSystem());
        TextField capitalField = getElement("capital");
        systemReportTable = getElement("systemReportTable");
        systemInfoTable = getElement("systemInfoTable");
        leversBox = getElement("lever");
        callsBox = getElement("call");
        typesBox = getElement("orderType");
        profitsDaysBox = getElement("orderProfitDays");
        ordersDaysBox = getElement("orderDays");
        openIndicatorBox = getElement("openIndicator");
        closeIndicatorBox = getElement("closeIndicator");
        quotationsBox = getElement("quotationsChooser");
        optimalFGraphic = getElement("optimalFGraphic");
        daysPercentProfitGraphic = getElement("daysPercentProfitGraphic");
        NumberAxis xAxis = (NumberAxis) optimalFGraphic.getXAxis();
        xAxis.setLowerBound(1);
        xAxis.setUpperBound(100);
        xAxis.setTickUnit(1);
        ChoiceBoxHelpers.initChoiceBox(leversBox, model.levers, model.leverReference);
        ChoiceBoxHelpers.initChoiceBox(callsBox, model.calls, model.callReference);
        ChoiceBoxHelpers.initChoiceBox(typesBox, model.types, model.typeReference);
        ChoiceBoxHelpers.initChoiceBox(profitsDaysBox, model.profitsDays, model.profitDaysReference);
        ChoiceBoxHelpers.initChoiceBox(ordersDaysBox, model.ordersDays, model.orderDaysReference);
        ChoiceBoxHelpers.initChoiceBox(openIndicatorBox, model.openIndicators, model.openIndicatorReference);
        ChoiceBoxHelpers.initChoiceBox(closeIndicatorBox, model.closeIndicators, model.closeIndicatorReference);
        ChoiceBoxHelpers.initChoiceModelBox(quotationsBox, model.getMainModel().quotationsModels, model.quotationsReference, model::updateIndicators);
        TextField stopText = getElement("stopSize");
        capitalField.setText(String.valueOf(model.getCapital()));
        stopText.setText(String.valueOf(model.getStopSizeString()));
        stopText.setOnKeyTyped((event) -> model.setStopSize(((TextField) event.getSource()).getText()));
        TableHelpers.createColumnsForInfoAboutSystemTable(systemInfoTable);
        TableHelpers.createColumnsForSystemReportTable(systemReportTable);
        updateSystemResult();
    }

    private void deleteCurrentSettings() {
        model.getMainModel().deleteSettingsModel(model.getId());
    }

    private void updateTitle() {
        view.setText(model.toString());
    }

    @Override
    public void initSignals() {
        signals.put(MLSubscriber.UPDATE_SYSTEM_RESULT, this::updateSystemResult);
        signals.put(MLSubscriber.UPDATE_SYSTEM_RESULT_WITHOUT_F, this::updateSystemResultWithoutOptimalF);
        signals.put(MLSubscriber.UPDATE_INDICATORS_FOR_SYSTEMS, this::updateIndicators);
        signals.put(MLSubscriber.UPDATE_SYSTEMS_QUOTATIONS_LIST, this::updateQuotationsList);
        signals.put(MLSubscriber.UPDATE_TITLE, this::updateTitle);
    }

    private void updateSystemResult() {
        updateSystemResultTables();
        updateOptimalFGraphic();
        updateDaysOfWeekGraphic();
        updateYearsGraphic();
    }

    private void updateSystemResultWithoutOptimalF() {
        updateSystemResultTables();
        updateDaysOfWeekGraphic();
        updateYearsGraphic();
    }

    private void updateQuotationsList() {
        ChoiceBoxHelpers.updateChoiceModelBox(quotationsBox, model.getMainModel().quotationsModels, model.quotationsReference);
    }

    private void updateIndicators() {
        ChoiceBoxHelpers.updateChoiceBox(openIndicatorBox, model.openIndicators, model.openIndicatorReference);
        ChoiceBoxHelpers.updateChoiceBox(closeIndicatorBox, model.closeIndicators, model.closeIndicatorReference);
    }

    private void updateDaysOfWeekGraphic() {
        BarChartHelpers.fillIntegerBarChart(daysPercentProfitGraphic, "Days Profit", model.getPercentProfitOfDays());
    }

    private void updateYearsGraphic() {

    }

    private void updateOptimalFGraphic() {
        NumberAxis xAxis = (NumberAxis) optimalFGraphic.getXAxis();
        xAxis.setLowerBound(0);
        xAxis.setUpperBound(100);
        xAxis.setTickUnit(1);
        NumberAxis yAxis = (NumberAxis) optimalFGraphic.getYAxis();
        yAxis.setLowerBound(0);
        yAxis.setUpperBound(model.getFinalBalance());
        yAxis.setTickUnit(model.getFinalBalance());
        AreaChartHelpers.fillOneIntegerSeriesAreaChart(optimalFGraphic, "Capital", "percent", model::getBalance);
    }

    private void updateSystemResultTables() {
        TableHelpers.fillSystemResult(systemReportTable, model.getSystemResults(), model.scores);
        TableHelpers.fillInfoAboutSystem(systemInfoTable, model.getSystemInfo());
    }

    @Override
    public Node getView() {
        return view;
    }
}
