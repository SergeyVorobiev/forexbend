package com.vorobiev.ui.view;

import com.vorobiev.model.SystemListModel;
import com.vorobiev.ui.helpers.LineChartHelpers;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;

public class SystemListView extends View {
    private LineChart profitPercentGraphic;
    public final SystemListModel model;

    public SystemListView(String pathToLayout, SystemListModel model) {
        super(pathToLayout, model);
        this.model = model;
        profitPercentGraphic = getElement("systemListGraphic");
        updateGraphic();
    }

    private void updateGraphic() {
        NumberAxis xAxis = (NumberAxis) profitPercentGraphic.getXAxis();
        xAxis.setLowerBound(1);
        xAxis.setUpperBound(200);
        xAxis.setTickUnit(1);
        NumberAxis yAxis = (NumberAxis) profitPercentGraphic.getYAxis();
        yAxis.setLowerBound(0);
        yAxis.setUpperBound(100);
        yAxis.setTickUnit(1);
        LineChartHelpers.fillOneIntegerSeriesLineChart(profitPercentGraphic, "Capital", "percent", model::getPercentProfit);
    }

    @Override
    public void initSignals() {
        signals.put(MLSubscriber.UPDATE_SYSTEM_MONITOR_GRAPHIC, this::updateGraphic);
    }

    @Override
    public Node getView() {
        return layout;
    }
}
