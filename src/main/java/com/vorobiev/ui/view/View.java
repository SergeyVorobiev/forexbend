package com.vorobiev.ui.view;

import com.vorobiev.model.Model;
import com.vorobiev.ui.helpers.FXWindow;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;

import java.util.TreeMap;

public abstract class View implements Subscriber {
    protected FXMLLoader loader;
    protected Parent layout;
    protected final TreeMap<Integer, Runnable> signals = new TreeMap();

    public View(String pathToLayout, Model model) {
        loader = FXWindow.getLoader(pathToLayout);
        layout = FXWindow.getLayout(loader);
        model.subscribers.add(this);
        initSignals();
    }

    public View(Parent layout, Model model) {
        this.layout = layout;
        model.subscribers.add(this);
        initSignals();
    }

    protected abstract void initSignals();

    @Override
    public void update(int signal) {
        Runnable run = signals.get(signal);
        if (run != null)
            run.run();
    }

    public abstract Node getView();

    public <T> T getElement(String selector) {
        return (T) layout.lookup("#" + selector);
    }
}
