package com.vorobiev.entity;

import com.vorobiev.model.SettingsModel;

import java.io.Serializable;

public class SimpleCloser implements OrderCloser, Serializable {
    private boolean isUp;
    public static final int CLOSE_ORDER = 0;
    public static final int STOP_LOSE = 1;
    public static final int CONTINUE = 2;
    public final String closeIndicator;
    public final int orderDays;
    public final int stopSize;

    public SimpleCloser(SettingsModel settings) {
        this.isUp = settings.getType();
        this.closeIndicator = settings.getCloseIndicator();
        this.orderDays = settings.getOrderDays();
        this.stopSize = settings.getStopSize();
    }

    @Override
    public int isClose(OrderResult order, Bar bar) {
        updateMin(order, bar);
        if (checkStop(order)) {
            order.close(bar);
            return STOP_LOSE;
        }
        if (order.getTicks() > 0) {
            order.close(bar);
            return CLOSE_ORDER;
        }
        if (!closeIndicator.equals("none")) {
            if (bar.getPredictableIndicator(closeIndicator) == 1.0f) {
                order.close(bar);
                return CLOSE_ORDER;
            }
        }
        if (orderDays == order.getDaysIn()) {
            order.close(bar);
            return CLOSE_ORDER;
        }
        return CONTINUE;
    }

    private void updateMin(OrderResult order, Bar bar) {
        int curMin;
        Bar startBar = order.bars.get(0);
        if (isUp) {
            order.setTicks(bar.close - startBar.open);
            curMin = bar.min - startBar.open;
        } else {
            order.setTicks(startBar.open - bar.close);
            curMin = startBar.open - bar.max;
        }
        if (order.getMin() > curMin)
            order.setMin(curMin);
    }

    private boolean checkStop(OrderResult order) {
        if (stopSize == 0)
            return false;
        if (order.getMin() <= stopSize) {
            order.setTicks(stopSize);
            return true;
        }
        return false;
    }
}
