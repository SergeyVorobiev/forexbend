package com.vorobiev.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class SystemInfo implements Serializable {
    public final OrderCloser closer;
    public final long uniqNumberOfQuotations;
    private float startCapital;
    private String openIndicator;
    private int optimalF = 0;
    private float twr = 0;
    private float z = 0;
    private float correlation = 0;
    private int countOrders = 0;
    private int averageGeom = 0;
    private int profitCount = 0;
    private int loseCount = 0;
    private float percentProfit = 0;
    private int maxProfit = 0;
    private int maxLose = 0;
    private float averProfit = 0;
    private float averLose = 0;
    private float mathExpect = 0;
    private float averageGeomOrder = 0;
    private int maxMin = 0;
    private int[] profitPercentByDaysOfWeek = new int[5];
    private ArrayList<Integer> percentProfitMap = new ArrayList();
    private TreeMap<Integer, Integer> sumProfitByYear = new TreeMap();

    public SystemInfo(OrderCloser closer, float startCapital, String openIndicator, long uniqNumberOfQuotations) {
        this.closer = closer;
        this.startCapital = startCapital;
        this.openIndicator = openIndicator;
        this.uniqNumberOfQuotations = uniqNumberOfQuotations;
    }

    public void compute(ArrayList<OrderResult> resultsOrigin, ArrayList<Score> scores) {
        if (resultsOrigin.isEmpty())
            return;
        ArrayList<OrderResult> results = new ArrayList(resultsOrigin);
        if (results.get(results.size() - 1).getCloseDate() == null)
            results.remove(resultsOrigin.size() - 1);
        if (results.isEmpty())
            return;

        ArrayList<OrderResult> profits = getProfitOrders(results);
        ArrayList<OrderResult> loses = getLossOrders(results);
        this.optimalF = scores.isEmpty() ? 0 : scores.get(0).percent;
        this.twr = scores.isEmpty() ? 0 : scores.get(scores.size() - 1).balance / startCapital;
        this.z = 0;
        this.correlation = 0;
        this.countOrders = results.size();
        this.averageGeom = 0;
        this.profitCount = profits.size();
        this.loseCount = loses.size();
        this.percentProfit = 100.0f / results.size() * profits.size();
        if (!profits.isEmpty())
            this.maxProfit = profits.stream().max((result1, result2) -> result1.getTicks() - result2.getTicks()).get().getTicks();
        if (!loses.isEmpty())
            this.maxLose = loses.stream().min((result1, result2) -> result1.getTicks() - result2.getTicks()).get().getTicks();
        int sum = 0;
        for (OrderResult result : profits) {
            sum += result.getTicks();
        }
        this.averProfit = (float) sum / profits.size();
        sum = 0;
        for (OrderResult result : loses) {
            sum += result.getTicks();
        }
        this.averLose = (float) sum / loses.size();
        this.mathExpect = 0;
        this.averageGeomOrder = 0;
        if (!loses.isEmpty())
            this.maxMin = loses.stream().min((result1, result2) -> result1.getMin() - result2.getMin()).get().getMin();
        calculatePercentOfProfitByDaysOfWeek(results);
        calculateSumProfitByYear(results);
        calculatePercentProfitMap(results);
    }

    public String getOpenIndicator() {
        return openIndicator;
    }

    private void calculatePercentProfitMap(ArrayList<OrderResult> orders) {
        percentProfitMap.clear();
        int profit = 0;
        int lose = 0;
        for (OrderResult order : orders) {
            if (order.getTicks() > 0) profit++;
            else lose++;
            int result = (int) ((float) profit / (float) (profit + lose) * 100.0f);
            percentProfitMap.add(result);
            if (percentProfitMap.size() > 200)
                percentProfitMap.remove(0);
        }
    }

    private void calculateSumProfitByYear(ArrayList<OrderResult> orders) {
        sumProfitByYear.clear();
        for (OrderResult r : orders) {
            int year = r.getOpenDate().year;
            int ticks = r.getTicks();
            Integer sum = sumProfitByYear.get(year);
            if (sum == null)
                sumProfitByYear.put(year, ticks);
            else
                sumProfitByYear.put(year, sum + ticks);
        }
    }

    private void calculatePercentOfProfitByDaysOfWeek(ArrayList<OrderResult> orders) {
        int daysOfWeek = 5;
        int[] summ = new int[daysOfWeek];
        int[] profit = new int[daysOfWeek];
        for (OrderResult r : orders) {
            int index = r.getOpenDate().dayOfWeek - 1;
            if (r.getTicks() > 0)
                profit[index]++;
            summ[index]++;
        }
        for (int i = 0; i < daysOfWeek; i++)
            profitPercentByDaysOfWeek[i] = (int) ((float) profit[i] / (float) summ[i] * 100);
    }

    private static ArrayList<OrderResult> getProfitOrders(ArrayList<OrderResult> orders) {
        return orders.stream().filter((result) -> result.getTicks() > 0).collect(Collectors.toCollection(ArrayList::new));
    }

    private static ArrayList<OrderResult> getLossOrders(ArrayList<OrderResult> orders) {
        return orders.stream().filter((result) -> result.getTicks() <= 0).collect(Collectors.toCollection(ArrayList::new));
    }

    public int getOptimalF() {
        return optimalF;
    }

    public float getTwr() {
        return twr;
    }

    public float getZ() {
        return z;
    }

    public float getCorrelation() {
        return correlation;
    }

    public int getCountOrders() {
        return countOrders;
    }

    public int getAverageGeom() {
        return averageGeom;
    }

    public int getProfitCount() {
        return profitCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public float getPercentProfit() {
        return percentProfit;
    }

    public int getMaxProfit() {
        return maxProfit;
    }

    public int getMaxLose() {
        return maxLose;
    }

    public float getAverProfit() {
        return averProfit;
    }

    public float getAverLose() {
        return averLose;
    }

    public float getMathExpect() {
        return mathExpect;
    }

    public float getAverageGeomOrder() {
        return averageGeomOrder;
    }

    public int getMaxMin() {
        return maxMin;
    }

    public int[] getProfitPercentByDaysOfWeek() {
        return profitPercentByDaysOfWeek;
    }

    public ArrayList<Integer> getPercentProfitMap() {
        return percentProfitMap;
    }

    public TreeMap<Integer, Integer> getSumProfitByYear() {
        return sumProfitByYear;
    }

}
