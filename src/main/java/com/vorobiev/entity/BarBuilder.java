package com.vorobiev.entity;

import java.util.ArrayList;
import java.util.TreeSet;

public class BarBuilder {
    private ArrayList<int[]> data = new ArrayList();
    private ArrayList<Integer> volumes = new ArrayList();
    private ArrayList<BarDate> dates = new ArrayList();

    public void addData(int[] data, BarDate date, int volume, boolean isLast, TreeSet<Bar> set) {

        if (dates.isEmpty()) {
            this.dates.add(date);
            this.data.add(data.clone());
            this.volumes.add(volume);
            if (isLast)
                set.add(formBar());
        } else if (this.dates.get(0).year == date.year && this.dates.get(0).month == date.month && this.dates.get(0).dayOfMonth == date.dayOfMonth) {
            this.data.add(data.clone());
            this.dates.add(date);
            this.volumes.add(volume);
            if (isLast)
                set.add(formBar());
        } else {
            set.add(formBar());
            clear();
            this.dates.add(date);
            this.data.add(data.clone());
            this.volumes.add(volume);
            if (isLast)
                set.add(formBar());
        }
    }

    public void clear() {
        this.dates.clear();
        this.data.clear();
        this.volumes.clear();
    }

    private Bar formBar() {
        ArrayList<InnerBar> innerBars = new ArrayList();
        if (data.size() > 1) {
            int i = 0;
            for (int[] ss : data) {
                innerBars.add(new InnerBar(ss[0], ss[1], ss[2], ss[3], volumes.get(i), dates.get(i++)));
            }
        }
        return new Bar(data.get(0)[0], getMax(), getMin(), data.get(data.size() - 1)[3], dates.get(0), getVolume(), innerBars);
    }

    private int getVolume() {
        int volume = 0;
        for (float v : volumes) {
            volume += v;
        }
        return volume;
    }

    private int getMax() {
        int max = Integer.MIN_VALUE;
        for (int[] ss : data) {
            if (max < ss[1])
                max = ss[1];
        }
        return max;
    }

    private int getMin() {
        int min = Integer.MAX_VALUE;
        for (int[] ss : data) {
            if (min > ss[2])
                min = ss[2];
        }
        return min;
    }
}
