package com.vorobiev.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

public class Bar implements Serializable, Comparable<Bar> {
    public final int open;
    public final int close;
    public final int max;
    public final int min;
    public static final int width = 5;
    public final int heightBody;
    public final int heightPick;
    public final boolean isUp;
    public final BarDate date;
    public final float volume;
    public static int i = 0;
    public int k;
    private final HashMap<String, Float> arithmeticIndicators = new LinkedHashMap();
    private final HashMap<String, Float> predictableIndicators = new LinkedHashMap();
    public final ArrayList<InnerBar> innerBars;

    public Bar(int open, int max, int min, int close, BarDate date, float volume, ArrayList<InnerBar> innerBars) {
        this.open = open;
        this.close = close;
        this.min = min;
        this.max = max;
        this.date = date;
        this.volume = volume;
        this.innerBars = innerBars;
        this.heightBody = Math.abs(close - open);
        this.heightPick = max - min;
        isUp = close - open > 0;
        k = i;
        i++;
    }

    public Float setArithmeticIndicator(String name, Float value) {
        return arithmeticIndicators.put(name, value);
    }

    public Float setPredictableIndicator(String name, Float value) {
        return predictableIndicators.put(name, value);
    }

    public Float getPredictableIndicator(String name) {
        return predictableIndicators.get(name);
    }

    public Float getArithmeticIndicator(String name) {
        return arithmeticIndicators.get(name);
    }

    public Set<String> getPredictableIndicatorNames() {
        return predictableIndicators.keySet();
    }

    public Set<String> getArithmeticIndicatorNames() {
        return arithmeticIndicators.keySet();
    }

    @Override
    public int compareTo(Bar o) {
        return this.date.compareTo(o.date);
    }
}
