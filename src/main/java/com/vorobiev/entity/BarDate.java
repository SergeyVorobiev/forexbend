package com.vorobiev.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class BarDate implements Serializable, Comparable<BarDate> {
    public final int dayOfWeek;
    public final int dayOfMonth;
    public final int hours;
    public final int month;
    public final int year;
    private static final Calendar calendar = new GregorianCalendar();

    public BarDate(Date date) {
        calendar.setTime(date);
        hours = calendar.get(Calendar.HOUR_OF_DAY);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public int compareTo(BarDate o) {
        if (o == null)
            throw new RuntimeException("Date on bar can not be null.");
        if (year > o.year)
            return 1;
        else if (year < o.year)
            return -1;
        else if (month > o.month)
            return 1;
        else if (month < o.month)
            return -1;
        else if (dayOfMonth > o.dayOfMonth)
            return 1;
        else if (dayOfMonth < o.dayOfMonth)
            return -1;
        else if (hours > o.hours)
            return 1;
        else if (hours < o.hours)
            return -1;
        return 0;
    }

    @Override
    public String toString() {
        return dayOfMonth + "." + month + "." + year;
    }
}
