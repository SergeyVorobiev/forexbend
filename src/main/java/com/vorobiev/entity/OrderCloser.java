package com.vorobiev.entity;

public interface OrderCloser {
    int isClose(OrderResult order, Bar bar);
}
