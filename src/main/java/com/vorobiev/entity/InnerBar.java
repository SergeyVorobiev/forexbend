package com.vorobiev.entity;

import java.io.Serializable;

public class InnerBar implements Serializable {
    public final int max;
    public final int min;
    public final int open;
    public final int close;
    public final int volume;
    public final BarDate date;

    public InnerBar(int open, int max, int min, int close, int volume, BarDate date) {
        this.open = open;
        this.max = max;
        this.min = min;
        this.close = close;
        this.volume = volume;
        this.date = date;
    }
}
