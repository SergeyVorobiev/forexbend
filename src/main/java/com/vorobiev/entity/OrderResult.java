package com.vorobiev.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderResult implements Serializable {
    private int daysIn;
    private int open;
    private BarDate openDate;
    private int close = 0;
    private BarDate closeDate = null;

    public final ArrayList<Bar> bars = new ArrayList();

    private int min = 0;

    private int ticks = 0;

    public OrderResult(int open, BarDate openDate) {
        daysIn = 0;
        this.openDate = openDate;
        this.open = open;
    }

    public OrderResult addDay(Bar bar) {
        daysIn++;
        bars.add(bar);
        return this;
    }

    public OrderResult close(Bar bar) {
        closeDate = bar.date;
        close = bar.close;
        return this;
    }

    public int getDaysIn() {
        return daysIn;
    }

    public int getOpen() {
        return open;
    }

    public BarDate getOpenDate() {
        return openDate;
    }

    public int getClose() {
        return close;
    }

    public BarDate getCloseDate() {
        return closeDate;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setTicks(int ticks) {
        this.ticks = ticks;
    }

    public int getMin() {
        return min;
    }

    public int getTicks() {
        return ticks;
    }
}
