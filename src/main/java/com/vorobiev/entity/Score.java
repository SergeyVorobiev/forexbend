package com.vorobiev.entity;

import java.io.Serializable;

public class Score implements Serializable {
    public final int bet;
    public final float earnings;
    public final float balance;
    public final int percent;

    public Score(int bet, float earnings, float balance, int percent) {
        this.bet = bet;
        this.earnings = earnings;
        this.balance = balance;
        this.percent = percent;
    }
}
