package com.vorobiev.entity;

import java.util.ArrayList;

public class Engine {
    public static final int ONLY_CLOSE = 0;
    public static final int ONLY_OPEN = 1;
    public static final int CLOSE_AND_OPEN = 2;

    public static void runAll(ArrayList<Bar> bars, ArrayList<OrderResult> results, SystemInfo info) {
        results.clear();
        for (int i = 1; i < bars.size(); i++) {
            Bar lastBar = bars.get(i - 1);
            int indicator = lastBar.getPredictableIndicator(info.getOpenIndicator()).intValue();
            Bar curBar = bars.get(i);
            openUpdateClose(curBar, results, info.closer, indicator);
        }
        System.out.println(results.size());
    }

    public static void openUpdateClose(Bar curBar, ArrayList<OrderResult> results, OrderCloser closer, int indicator) {

        // We have an opened order try to close it.
        if (!results.isEmpty()) {
            OrderResult result = results.get(results.size() - 1);
            if (result.getCloseDate() == null) {
                closer.isClose(result.addDay(curBar), curBar);
                return;
            }
        }

        // We do not have an opened order try to open it and close.
        if (indicator == 1) {
            OrderResult result = new OrderResult(curBar.open, curBar.date);
            results.add(result);
            closer.isClose(result.addDay(curBar), curBar);
        }
    }
}
