package com.vorobiev.entity;

import java.io.Serializable;

public class UserIndicatorInfo implements Serializable {
    public final String name;
    public final String formula;
    public final boolean isPredictable;

    public UserIndicatorInfo(String name, String formula, boolean isPredictable) {
        this.name = name;
        this.formula = formula;
        this.isPredictable = isPredictable;
    }
}
