package com.vorobiev.main;

import com.vorobiev.model.MainModel;
import com.vorobiev.ui.windows.MainWindow;
import javafx.application.Application;
import javafx.stage.Stage;

public class StartApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        new MainWindow(primaryStage, new MainModel(), "ML", false).show();
    }
}
